<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

$this->get('admin-panel/login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('admin-panel/login', 'Auth\LoginController@login');
//$this->post('admin-panel/logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
//$this->get('admin-panel/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');


Route::group(['middleware'=>'auth'],function (){

    Route::resource('admin-panel/users', 'AdminPanel\UserController');

    Route::resource('admin-panel/roles', 'AdminPanel\RoleController');

    Route::resource('admin-panel/products', 'AdminPanel\ProductController');

    Route::resource('admin-panel/images', 'AdminPanel\ImageController');

    Route::resource('admin-panel/shipments', 'AdminPanel\ShipmentController');

    Route::resource('admin-panel/postmen', 'AdminPanel\PostmanController');

    Route::resource('admin-panel/transactions', 'AdminPanel\TransactionController');

    Route::resource('admin-panel/allsells', 'AdminPanel\AllsellController');

    Route::resource('admin-panel/comments', 'AdminPanel\CommentController');

    Route::resource('admin-panel/images', 'AdminPanel\ImageController');

    Route::resource('admin-panel/shipmentTypes', 'AdminPanel\ShipmentTypeController');

    Route::resource('admin-panel/paymentTypes', 'AdminPanel\PaymentTypeController');

    Route::resource('admin-panel/productTypes', 'AdminPanel\ProductTypeController');

    Route::resource('admin-panel/posts', 'AdminPanel\PostController');

    Route::get('/admin-panel', 'AdminPanel\AdminPanelController@index');

});

//Route::get('/home', 'HomeController@index');

//API

Route::get('/buyershow/{id}','Front\API\BuyerProductController@show');
Route::get('/buyershowall', 'Front\API\BuyerProductController@showall');
Route::get('/comm/{id}', 'Front\api\BuyerProductController@showcomments');
Route::get('/clientshowpost/{id}', 'Front\API\ClientPostController@show');
Route::get('/clientshowallpost', 'Front\API\ClientPostController@showall');

Route::get('/cartdtail', 'Front\API\CartDtailController@showall');


Route::get('login', 'Front\LoginController@index');

//Route::get('register', 'Front\RegisterController@index');

Route::get('/', 'Front\HomeController@index');

Route::get('address', 'Front\AddressController@index');

Route::get('reviewaddress', 'Front\ReviewAddressController@index');

Route::get('calculations', 'Front\CalculationsController@index');

Route::get('cart', 'Front\Cart1Controller@index');

Route::get('review', 'Front\ReviewController@index');

Route::get('paymentsuccess', 'Front\PayMentSuccessController@index');

Route::get('product', 'Front\ProductController@index');

Route::get('product/{id}', 'Front\ProductController@index');

Route::get('profile', 'Front\ProfileController@index');

Route::get('blog', 'Front\BlogController@index');

//Route::get('blog/post', 'Front\PostController@index');
Route::get('blog/post/{id}', 'Front\PostController@show');

Route::get('finalstep', 'Front\FinalStepController@index');

//summernote store route
Route::post('/summernote','SummernoteController@store')->name('summernotePersist');

//summernote display route
Route::get('/summernote_display','SummernoteController@show')->name('summernoteDispay');

Route::resource('posts', 'PostController');

Route::resource('postTags', 'PostTagController');





//
Route::get('/profile/profile',function(){
    return view('front.Profile.profile');
});
Route::get('/profile/information',function(){
    return view('front.Profile.information');
});
Route::get('/profile/address',function(){
    return view('front.Profile.address');
});
Route::get('/profile/bookmark',function(){
    return view('front.Profile.bookmark');
});
Route::get('/profile/changeAvatar',function(){
    return view('front.Profile.changeAvatar');
});
Route::get('/profile/changePassword',function(){
    return view('front.Profile.changePassword');
});
Route::get('/profile/order',function(){
    return view('front.Profile.order');
});
Route::get('/profile/question',function(){
    return view('front.Profile.question');
});
Route::get('/profile/requestreturn',function(){
    return view('front.Profile.requestreturn');
});