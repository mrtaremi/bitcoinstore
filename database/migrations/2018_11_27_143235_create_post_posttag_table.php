<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostPosttagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_posttag', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->integer('post_id')->unsigned();
            $table->foreign('posttag_id')->references('id')->on('posttags');
            $table->integer('posttag_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_posttag');
    }
}
