$(document).ready(function () {
    $(".boxProf4 div").click(function () {
        $(".boxProf4 div").removeClass("activeClc");
        $(this).addClass("activeClc");
    });
    $(location).attr('href');
    var pathname = window.location.pathname;
    if (pathname == "/profile")
        $("#profile").addClass("ac");
    if (pathname == "/profile/bookmark")
        $("#bookmark").addClass("ac");
    if (pathname == "/profile/information")
        $("#information").addClass("ac");
    if (pathname == "/profile/order")
        $("#order").addClass("ac");
    if (pathname == "/profile/question")
        $("#question").addClass("ac");
    if (pathname == "/profile/requestreturn")
        $("#requestreturn").addClass("ac");
    if (pathname == "/profile/address")
        $("#address").addClass("ac");


    if ($(window).width() < 768) {
        $(".redirect .items").hide();
        $("#headerRedirect").css("border","none");

        var clicked = true;
        $("#headerRedirect").click(function () {
            if (clicked) {
                $(".redirect .items").show(500);
                $("#headerRedirect").css("border-bottom","1px solid #000");
                $("#triangle-down").css("transform","rotate(180deg)");
                clicked = false;
            } else {
                $("#headerRedirect").css("border","none");

                 $(".redirect .items").hide(500);
                $("#triangle-down").css("transform","rotate(0deg)");
                clicked = true;
            }
        });
    }
    else{
        $("#triangle-down").hide();
    }


});