<!-- Post Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post_id', 'Post Id:') !!}
    {!! Form::number('post_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Reply Of Comment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reply_of_comment', 'Reply Of Comment:') !!}
    {!! Form::number('reply_of_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Likes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('likes', 'Likes:') !!}
    {!! Form::number('likes', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Accepted Field -->
<div class="form-group col-sm-6">
    {!! Form::label('accepted', 'Accepted:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('accepted', false) !!}
        {!! Form::checkbox('accepted', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('comments.index') !!}" class="btn btn-default">Cancel</a>
</div>
