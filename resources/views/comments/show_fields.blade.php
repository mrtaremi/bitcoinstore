<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $comment->id !!}</p>
</div>

<!-- Post Id Field -->
<div class="form-group">
    {!! Form::label('post_id', 'Post Id:') !!}
    <p>{!! $comment->post_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $comment->product_id !!}</p>
</div>

<!-- Reply Of Comment Field -->
<div class="form-group">
    {!! Form::label('reply_of_comment', 'Reply Of Comment:') !!}
    <p>{!! $comment->reply_of_comment !!}</p>
</div>

<!-- Likes Field -->
<div class="form-group">
    {!! Form::label('likes', 'Likes:') !!}
    <p>{!! $comment->likes !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $comment->value !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $comment->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $comment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $comment->updated_at !!}</p>
</div>

<!-- Accepted Field -->
<div class="form-group">
    {!! Form::label('accepted', 'Accepted:') !!}
    <p>{!! $comment->accepted !!}</p>
</div>

