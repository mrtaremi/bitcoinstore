<table class="table table-responsive" id="postmen-table">
    <thead>
        <tr>
            <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>Phone Number</th>
        <th>Const Number</th>
        <th>Identity Number</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($postmen as $postman)
        <tr>
            <td>{!! $postman->first_name !!}</td>
            <td>{!! $postman->last_name !!}</td>
            <td>{!! $postman->address !!}</td>
            <td>{!! $postman->phone_number !!}</td>
            <td>{!! $postman->const_number !!}</td>
            <td>{!! $postman->identity_number !!}</td>
            <td>
                {!! Form::open(['route' => ['postmen.destroy', $postman->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('postmen.show', [$postman->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('postmen.edit', [$postman->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>