@extends('layouts.MasterPage') @section('SubCss')
<title>2بیتکوین - محصول</title>
<style>
  .imgProduct {

    width: 100%;
    height: auto;
    border: 1px solid gray;
    border-radius: 3px;
    margin-right: 10px;
    margin-top: 0;
    margin-bottom: 15px;
    padding-right: 0px !important;
    margin-right: 0px !important
  }

  .paragraphDetails {
    font-size: 17px;
    width: 80%;
  }

  hr {
    width: 80%;
  }

  #spaceTab {
    margin-top: 20px !important;
  }


  /*tab1 */

  .imgContent {
    width: 80%;
    height: 300px;
    border-radius: 20px;
    border: 1px solid gray;
  }

  .contText2 {
    line-height: 25px;
    text-align: justify;
    font-size: 17px;
    padding: 20px;
  }

  .imgCustom {
    height: auto;
    margin-top: -15%;
    border-radius: 25px;
    display: inline;
  }

  #contText1 {
    width: 80%;
    margin: 0 auto;
    line-height: 30px;
    margin-bottom: 7%;
  }

  .titleBanner {
    padding-left: 20px;
    padding-right: 20px;
    padding-bottom: 20px;
    padding-top: 10px;
    margin-right: 10px;
  }

  .banner h4 {
    color: #fff;
  }

  .banner {
    width: 100%;
    height: 200px;
    background-image: linear-gradient(190deg, #c2e59C 30%, #64B3F4 100%) !important;
    border: 1px solid gray;
    border-radius: 7px;
    margin-top: 20px;
  }

  /*tab1 */

  /* tab2 */

  .marginRight10px {
    margin-right: 10px;
  }

  .boxText {
    font-size: 18px;
    text-align: right;
  }

  .boxDiemn {

    height: 60px;
    background-color: #F8F8F8;
    border: 1px solid gray;
    padding: 10px;
  }

  /* tab2 */

  /* tab4 */

  .borderOfBtn {
    border: 1px solid gray;
  }

  .formSend2 {
    border-radius: 0px;
    border-bottom-left-radius: 12px !important;
    border-bottom-right-radius: 12px !important;
    font-size: 16px;
    -webkit-box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
    -moz-box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
    box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
    resize: none;
  }

  .formSend1 {
    border-radius: 0px;
    border-top-right-radius: 12px !important;
    border-top-left-radius: 12px !important;
    font-size: 16px;
    -webkit-box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
    -moz-box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
    box-shadow: 0px 5px 23px 0px rgba(222, 222, 222, 1);
  }

  .boxForm {
    padding: 10px;
    height: auto;
    -webkit-box-shadow: 0px 1px 21px 3px rgba(171, 227, 197, 1);
    -moz-box-shadow: 0px 1px 21px 3px rgba(171, 227, 197, 1);
    box-shadow: 0px 1px 21px 3px rgba(171, 227, 197, 1);
  }

  .boxText2 {
    text-align: center;
    padding-top: 30px !important;
    font-size: 40px;
    font-weight: 400 !important;
  }

  /* tab4 */

  /* tab style header and body */

  .headerTab ul {
    width: 100%;
    height: 25px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.16);
    padding: 0;
    margin: 0;
  }

  .headerTab ul li {
    width: 25%;
    height: 25px;
    padding: 0;
    margin: 0;
    float: right;
    text-align: center;
    list-style-type: none;
    font-size: 0.8em;
    transition: 300ms;
    cursor: pointer;

  }

  .headerTab ul .ac {
    border-bottom: 2px solid #33CCFF;
    font-size: 0.9em;
    transition: 300ms;
  }

  .none {
    display: none;
  }

  /* tab style header and body */

  .handleOfBuy {
    color: white !important;
    border: 1px solid gray !important;
    margin-top: 20px !important;
    line-height: 40px;
    font-weight: 500;
  }

  @media (max-width: 768px) {
    .headerTab ul li {
      font-size: 0.8em;
    }
    .headerTab ul .ac {
      font-size: 0.9em;
    }
  }

  .comments .comment {
    border-radius: 20px;
    box-shadow: 0px 3px 5px #00000062;
    overflow: hidden;
  }

  .comments .comment span {
    padding-bottom: 3px;
  }

  .comments .comment .reply {
    height: auto;
    opacity: 0.4;
    transition: 1000ms;
  }

  .comments .comment .reply a {
    text-decoration: none;
    color: #000;
  }

  .comments .comment .reply:hover {
    opacity: 1;
  }

  .comments .comment .reply .imgReply {
    width: 50px;
    height: auto;
    border-radius: 50%;
    overflow: hidden;
    /* vertical-align: middle; */
    margin-top: 15px;
    margin-bottom: 15px;
    float: right;
    margin-right: 15px;
    border: 1px solid #00000043;

  }

  .comments .comment .reply .textBoxReply {
    /* float: right; */
    padding-top: 10px;
    margin-right: 80px;
    width: 60% !important;
    line-height: 25px;

  }




  .comments .comment .mainCmments .imgmain {
    width: 80px;
    height: auto;
    border-radius: 50%;
    overflow: hidden;
    /* vertical-align: middle; */
    margin-top: 15px;
    margin-bottom: 15px;
    float: right;
    margin-right: 15px;
    border: 1px solid #00000043;

  }

  .comments .comment .mainCmments .textBoxmain {
    /* float: right; */
    padding-top: 10px;
    margin-right: 105px;
    width: 85% !important;
    line-height: 25px;

  }

  .comments .comment .mainCmments .textBoxmain {
    /* float: right; */
    padding-top: 10px;
    margin-right: 105px;
    width: 85% !important;
    line-height: 25px;

  }

  .comments .comment .mainCmments .textBoxmain .borderx {

    border-left: 1px solid rgb(0, 0, 0);

  }

  .comments button{
    display: block;
    width: 80%;
    margin: 0 auto;
  }






  .moreBtn1 {
    width: 90%;
    margin: 0 auto;
  }

  .moreBtn1 div {
    width: 50%;
    float: left;
    margin: 0px;
  }

  .moreBtn1 div p {
    font-size: 0.7rem;
  }

  .moreBtn1 div p span {
    padding-bottom: 3px;
  }

  .moreBtn1 div p svg {
    margin-left: 8px;
  }

  .clickedBookmarked {
    fill: #000;
    stroke: black;
  }
  .clickedBookmarked1 {
    fill: #f00;
    stroke: black;
  }

  .clickedBookmarkedrep {
    fill: #FFD700;
    stroke: black;
  }

  .textmain {
    margin-bottom: 40px;
    margin-top: 20px;
  }

  .textmain1 {
    margin-bottom: 10px;
    margin-top: 10px;
  }

  #txtpad {
    padding-right: 3px;
  }


  @media (max-width: 768px) {
    .textmain {
      margin-left: 22%;
    }
    .textmain1 {
      /* margin-right: -20%;
      margin-top: 10%; */
    }
  }
</style>
@endsection @section('SubMain')
<div class="container-fluid allDetails ">
  <div class="row ">
    <div class="col-md-5 ">
      <div class="uk-position-relative uk-visible-toggle uk-light " uk-slideshow>

        <ul class="uk-slideshow-items " uk-height-viewport="offset-top: true; offset-bottom: 30 ">
          <li>
            <img id="avatar_image_path" src="/img/mine.jpg " alt=" " uk-cover>
          </li>
          <li>
            <img src="/img/mine.jpg " alt=" " uk-cover>
          </li>
          <li>
            <img src="/img/mine.jpg " alt=" " uk-cover>
          </li>
        </ul>

        <a class="uk-position-center-right uk-position-small text-dark " href="#
            " uk-slidenav-previous uk-slideshow-item="previous "></a>
        <a class="uk-position-center-left uk-position-small text-dark " href="#
            " uk-slidenav-next uk-slideshow-item="next "></a>

      </div>

    </div>
    <div class="col-md-7 ">
      <h4 id="name" class="fontBold "></h4>

      <hr class="bboo ">
      <p id="sub_description" class=" paragraphDetails "></p>
      <hr>
      <div class="row ">
        <div class="col-md-12 col-sm-12 col-12 ">
          <div class="uk-grid-small " uk-grid>
            <div class="uk-width-expand ">
              <span uk-icon="icon: bookmark " class="deleteBtn bookmarked "></span>
            </div>
            <div class="fontBold ">
              <span class="text-primary " id="price_per_unit"></span> </div>
          </div>
          <button id="buyBtn" class="btn btn-block handleOfBuy gradientX btn-lg">افزودن به سبد خرید</button>
        </div>
      </div>



    </div>
    <div class="col-md-12 " id="spaceTab ">
      <div>
        <br>
        <br>
        <div class="headerTab uk-margin-top">
          <ul>
            <li id="H1" class="ac">بررسی محصول</li>
            <li id="H2">مشخصات</li>
            <li id="H3">نظرات کاربران</li>
            <li id="H4">پرسش و پاسخ</li>
          </ul>
        </div>
        <div class="bodyTab">
          <br>
          <br>
          <div class="contentTab" id="B1">
            <div class="banner ">
              <h4 class="titleBanner fontBold ">
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم
              </h4>
              <h4 class="titleBanner ">
                لورم ایپسوم متن ساختگی با تولید
                <br> سادگی نامفهوم از صنعت چاپ
                <br> و با استفاده از طراحان گرافیک است
              </h4>
            </div>
            <div class="col-md-12 ">
              <p class="text-center uk-margin-medium-top fontBold ">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</p>
              <br>
              <br>
              <div id="contText1 ">
                <p class="text-center uk-text-justify ">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون
                  بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع
                  با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه
                  و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و
                  فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها
                  و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل
                  دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                </p>
              </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="gradientX ">
              <div class="col-md-6 offset-md-3 col-sm-8 offset-sm-2 ">
                <img src="img/bit.jpg " alt="bitCoin
                  " class="img-fluid imgCustom ">
              </div>
              <div class="row ">
                <div class="col-md-6 text-center uk-margin-xlarge-top ">
                  <p class="fontBold ">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</p>
                  <p class=" contText2 ">لورم ایپسوم متن ساخوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم
                    متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی با تولید
                    سادگتگیوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی
                    با تولید سادگوم متن ساختگی با تولید سادگوم متن ساختگی با تولید سادگوم متن سم متن ساختگی با تولید سادگوم
                    متن ساختگی با تولاختگی با تولید م متن ساختگی بم متن ساختگی با تولید سادگوم متن ساختگی با تولا تولید سام
                    متن ساختگی با تولید سادم متن ساختگی با تولید سادگوم متن ساختم متن ساختگی با تولید سادگوم متن ساختگی با
                    تولگی با تولگوم متن ساختگی با م متن ساختگی با تولید سادگوم متن ساختگی با تولتولدگوم متن ساختگی با تولسادگوم
                    متن ساختگی با تولید سادگs با تولید سادگی نامفهوم</p>
                </div>
                <div class="col-md-6 text-center uk-margin-xlarge-top ">
                  <img src="img/bit3.jpg " alt="bit2Coin " class="imgContent uk-margin-bottom ">
                </div>
              </div>
            </div>

          </div>

          <div class="contentTab" id="B2">
            <h4 class="fontBold uk-margin-medium-top ">لورم ایپسوم متن ساختگی</h4>
            <p class=" ">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</p>
            <h3 class="uk-margin-large-top ">جزئیات</h3>
            <div class="row ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">ابعاد</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="Dimensions" class=" boxText "></p>
              </div>
            </div>
            <div class="row uk-margin-top ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">وزن</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="weight" class=" boxText "></p>
              </div>
            </div>
            <div class="row uk-margin-top ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">رنگ ها</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="colors" class=" boxText "></p>
              </div>
            </div>
            <h3 class="uk-margin-large-top ">
              <p>قدرت پردازش</p>
            </h3>
            <div class="row uk-margin-top ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">مقدار ماین در ساعت</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="btc_per_hour" class=" boxText "></p>
              </div>
            </div>
            <div class="row uk-margin-top ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">مصرف برق دستگاه</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="poweruse" class=" boxText "></p>
              </div>
            </div>
            <div class="row uk-margin-top ">
              <div class="col-md-4 col-sm-4 col-4 boxDiemn marginRight10px ">
                <p class=" boxText ">مقدار گرم شدن در روز</p>
              </div>
              <div class="col-md-7 col-sm-7 col-7 boxDiemn marginRight10px ">
                <p id="gas" class=" boxText "> </p>
              </div>
            </div>

          </div>

          <div class="contentTab" id="B3">
            <div class="container">
              <h2 class="uk-text-primary text uk-margin-medium-bottom" id="goto">ارسال نظر</h2>
              <div class="row">
                <div class="col-md-12 comments">

                  <div class="inputComment comment writeComment uk-margin-medium-bottom uk-background-muted">

                    <div class="mainCmments mainCmments1">
                      <div class="imgmain">
                        <img src="img/12.jpg" alt="">
                      </div>
                      
                          <h4 class="uk-margin-medium-top">میکسا</h4>
                          <span class="uk-margin-small-right uk-text-meta uk-button-text">نظر خودت را بنویس</span>
                        <div class="col-md-12 col-12 uk-margin-small-top">
                          <textarea name="" id="txtComment" placeholder="متن پیام..."  rows="7" class="uk-textarea uk-width-1-1 textmain1"></textarea>
                        </div>
                      
                    </div>
                    <button class="uk-button uk-button-primary uk-margin-small-bottom">ارسال نظر</button>
                  </div>
                </div>
              </div>
              <div class="comments">

                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c1">
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                        alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>ممد</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">zxczxczxczxczxczxc
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>

                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c2">
                  <div class="reply">
                    <a href="#c1" uk-scroll>
                      <div class="imgReply">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                          alt="">
                      </div>
                      <div class="textBoxReply">

                        <p>ممد</p>
                        <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته
                          شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                        </p>

                      </div>
                    </a>
                  </div>
                  <hr>
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="https://is2-ssl.mzstatic.com/image/thumb/Purple30/v4/e1/27/30/e127305d-97be-eb78-f9c7-a3dc9ac061a9/source/512x512bb.jpg"
                        alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>مریم</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته شده
                        استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>

                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c3">
                  <div class="reply">
                    <a href="#c1" uk-scroll>
                      <div class="imgReply">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                          alt="">
                      </div>
                      <div class="textBoxReply">

                        <p>ممد</p>
                        <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته
                          شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                        </p>

                      </div>
                    </a>
                  </div>
                  <hr>
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgQXhbivolCosFWGZcPVBJmOJZ5SHFxO9J-SpTEj04fbTJ1n8p" alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>امیر</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته شده
                        استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>

                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c4">
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="https://store.playstation.com/store/api/chihiro/00_09_000/container/CA/en/999/UP1112-CUSA06917_00-AV00000000000016/1536293595000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000"
                        alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>تیرکس</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته شده
                        استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>

                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c5">
                  <div class="reply">
                    <a href="#c1" uk-scroll>
                      <div class="imgReply">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                          alt="">
                      </div>
                      <div class="textBoxReply">

                        <p>ممد</p>
                        <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته
                          شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                        </p>

                      </div>
                    </a>
                  </div>
                  <hr>
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9N12agWUTFIhrCo2JjILkuWIq-bH5cHRyOGh_5h85KnQq2lCaTg" alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>ماریو</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته شده
                        استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c6">
                  <div class="reply">
                    <a href="#c2" uk-scroll>
                      <div class="imgReply">
                        <img src="https://is2-ssl.mzstatic.com/image/thumb/Purple30/v4/e1/27/30/e127305d-97be-eb78-f9c7-a3dc9ac061a9/source/512x512bb.jpg"
                          alt="">
                      </div>
                      <div class="textBoxReply">

                        <p>مریم</p>
                        <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته
                          شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                        </p>

                      </div>
                    </a>
                  </div>
                  <hr>
                  <div class="mainCmments">
                    <div class="imgmain">
                      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfh91754-ZayjDY344zReUn_U3PHUsJ5zkrb8ABEt3flq9Lxpr" alt="">
                    </div>
                    <div class="textBoxmain">

                      <h4>احمد</h4>
                      <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                      <span class="borderx"></span>
                      <span class="uk-margin-medium-right uk-button-text uk-text-primary replyBtn"><a href="#goto" uk-scroll>پاسخ</a></span>
                      <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع کلی طرح سفارش گرفته شده
                        استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                      </p>

                    </div>
                    <div class="moreBtn1">
                      <div class="uk-align-left repbox">
                        <p class="text-right ">
                          <span class="repbtn" uk-icon="icon: warning"></span>
                          <span class="uk-button-text reptxt">گزارش تخلف</span>
                        </p>
                      </div>
                      <div class="uk-align-right">
                        <p class="text-left likebox">
                          <span class="likebtn" uk-icon="icon: heart"></span>
                          <span class="uk-button-text liketxt">522</span>
                        </p>

                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="contentTab" id="B4">
            <div class="row ">
              <div class="col-md-8 offset-md-2 ">
                <div class="boxForm uk-margin-medium-top ">
                  <p class="fontBold boxText2 uk-margin-right ">پیام خود را برای ما ارسال کنید</p>
                  <div class="row ">
                    <div class="col-md-10 ">
                      <p class=" text-center uk-margin-top ">در اولین فرصت به شما پاسخ خواهیم داد، باتشکر.</p>
                    </div>
                  </div>
                  <div class="row ">
                    <div class="col-md-8 col-sm-8 offset-sm-2 offset-md-2 ">
                      <input class="form-control form-control-lg
                  formSend1 " type="text " placeholder="نام خود را وارد کنید ">
                    </div>
                  </div>
                  <div class="row ">
                    <div class="col-md-8 col-sm-8 offset-sm-2 offset-md-2 ">
                      <div class="form-group ">
                        <label for="exampleFormControlTextarea1 "></label>
                        <textarea class="form-control formSend2 " id="exampleFormControlTextarea1 " rows="4 " placeholder="پیام خود را وارد کنید "></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row ">
                    <div class="col-md-8 col-sm-8 offset-sm-2 offset-md-2 ">
                      <button type="button " class="btn btn-primary btn-block borderOfBtn uk-margin-top uk-margin-medium-bottom
                  gradientBtn ">ارسال</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
  </div>
</div>


@endsection @section('SubScript')
<script>

  $(document).ready(function () {


    $.get("/buyershow/{{$id}}", function (data, status) {


      $("#avatar_image_path").attr("src", "/" + data.product.avatar_image_path);
      $("#sub_description").text(data.product.sub_description);
      $("#name").text(data.product.name);

      $("#Dimensions").text(data.product.dimensions);

      $("#weight").text(data.product.weight + " گرم ");
      $("#colors").text(data.product.colors);
      $("#poweruse").text(data.product.poweruse);
      $("#gas").text(data.product.gas);
      $("#btc_per_hour").text(data.product.btc_per_hour);
      $("#price_per_unit").text(colon(data.product.price_per_unit.toString()) + " تومان ");


    });

    var isBookmark = true;
    if (isBookmark) {
      $(".bookmarked ").ready(function () {
        $(this).find("svg polygon ").toggleClass("clickedBookmarked ", 1000);
        $(this).find("svg ").toggleClass("clickedForSize ", 1000);

      });
    }

    $(".bookmarked svg polygon ").addClass("defaultBokmarked ");
    $(".bookmarked svg ").addClass("clickForSize");
    $(".bookmarked ").click(function () {
      $(this).find("svg polygon ").toggleClass("clickedBookmarked ", 1000);
      $(this).find("svg ").toggleClass("clickedForSize ", 1000);
    });
    

    // Comments
    var like=12;
    $(".likebox").click(function () {
      $(this).find(".liketxt").text(like);
      $(this).find(".likebtn svg path").toggleClass("clickedBookmarked1");
      $(this).find(".liketxt").toggleClass("uk-text-primary");
      
    });
    $(".repbox").click(function () {
      $(this).find(".repbtn svg circle:nth-child(1)").attr("fill", "black");
      $(this).find(".repbtn svg circle:nth-child(2)").toggleClass("clickedBookmarkedrep");
      $(this).find(".reptxt").toggleClass("uk-text-primary");
      UIkit.notification({ message: 'گزارش با موفقیت ارسال شد', status: 'primary', pos: 'bottom-center' });
      
    });
    $(".reply").click(function () {
      var a = $(this).find("a").attr("href");
      $(a).css("box-shadow", "0px 5px 5px #00000062");
    });




    $(".replyBtn").click(function(){
      var name=$(this).parent().find("h4").text();
      var text=$(this).parent().find(".textmain").text();
      var img=$(this).parent().parent().find(".imgmain img").attr("src");
      var href=$(this).parent().parent().parent().attr("id");
      var a=$("<div class='reply'><a href='#"+href+"' uk-scroll><div class='imgReply'><img src="+img+"></div><div class='textBoxReply'><p>"+name+"</p><p class='textReply uk-text-truncate'>"+text+"</p></div></a></div><hr>");
     $(".inputComment .reply ,.inputComment hr ").remove();
      $(".mainCmments1").prepend(a);
    });
    // Comments

    // tab


    $(function () {

      var badge;

      if (checkCookie('cart')) {

        badge = getCookie('cart').split('|').length;

      }
      else {
        badge = 0
      }

      $(".badgeCustom ").text(badge);
      $('#buyBtn').click(function () {

        if (checkCookie('cart')) {

            c=getCookie('cart');
            cc=c.split('|');

            var index = cc.indexOf('{{$id}}');

            if (index == -1) {
                setCookie('cart', c + '|{{$id}}', 10);

                badge++;
                $(".badgeCustom ").text(badge);
                UIkit.notification({ message: 'با موفقیت به سبد خرید اضافه شد.', status: 'success', pos: 'bottom-center' })
                var buyBtn = $('#buyBtn');
                buyBtn.prop('disabled', true);
            }
            else {
                UIkit.notification({ message: 'کالا در سبد خرید موجود است !!', status: 'warning', pos: 'bottom-center' })
                var buyBtn = $('#buyBtn');
                buyBtn.prop('disabled', true);
            }

        }
        else {

          setCookie('cart', '{{$id}}', 10);

            badge++;
            $(".badgeCustom ").text(badge);
            UIkit.notification({ message: 'با موفقیت به سبد خرید اضافه شد.', status: 'success', pos: 'bottom-center' })
            var buyBtn = $('#buyBtn');
            buyBtn.prop('disabled', true);

        }
      });
    });




    $(".bodyTab .contentTab").addClass("none");
    $(".bodyTab #B1").removeClass("none");
    $("#H1").click(function () {
      $(".bodyTab .contentTab").fadeOut(300);
      $("#B1").fadeIn(300);
      $(".headerTab li").removeClass("ac");
      $(this).addClass("ac");
    });
    $("#H2").click(function () {
      $(".bodyTab .contentTab").fadeOut(300);
      $("#B2").fadeIn(300);
      $(".headerTab li").removeClass("ac");
      $(this).addClass("ac");
    });
    $("#H3").click(function () {
      $(".bodyTab .contentTab").fadeOut(300);
      $("#B3").fadeIn(300);
      $(".headerTab li").removeClass("ac");
      $(this).addClass("ac");
    });
    $("#H4").click(function () {
      $(".bodyTab .contentTab").fadeOut(300);
      $("#B4").fadeIn(300);
      $(".headerTab li").removeClass("ac");
      $(this).addClass("ac");
    });



  });


</script> @endsection