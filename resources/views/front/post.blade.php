<!doctype html>
<html lang="fa" dir="rtl">

<head>
    <title>بیتکوین1</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="img/coin_Tg0_icon.ico" type="image/x-icon">

    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />


    <!-- style -->
    <style>
        body {
            cursor: default;
        }

        .morePost {
            width: 90%;
            margin: 0 auto;
            height: auto;
            box-shadow: 0px 2px 3px #00000062;

        }

        .morePost:nth-child(1) {
            margin-top: 40px !important;

        }

        .morePost h4 {
            margin: 0px;
            margin-top: 10px;
            line-height: 30px;

        }

        .post {
            width: 95%;
            margin: 0 auto;

        }

        #moreBtn div {
            width: 33.333333%;
            float: left;
            margin: 0px;
        }

        #moreBtn div p {
            font-size: 0.9rem;
        }

        #moreBtn div p span {
            padding-bottom: 3px;
        }

        #moreBtn div p svg {
            margin-left: 8px;
        }

        #btnx {
            border-radius: 3px;
        }

        #btnx span {
            font-size: 1.2rem;
        }

        .comments .comment {
            border-radius: 20px;
            box-shadow: 0px 3px 5px #00000062;
            overflow: hidden;
        }

        .comments .comment span {
            padding-bottom: 3px;
        }

        .comments .comment .reply {
            height: auto;
            opacity: 0.4;
            transition: 1000ms;
        }
        .comments .comment .reply a{
            text-decoration: none;
            color: #000;
        }

        .comments .comment .reply:hover {
            opacity: 1;
        }

        .comments .comment .reply .imgReply {
            width: 50px;
            height: auto;
            border-radius: 50%;
            overflow: hidden;
            /* vertical-align: middle; */
            margin-top: 15px;
            margin-bottom: 15px;
            float: right;
            margin-right: 15px;
            border: 1px solid #00000043;

        }

        .comments .comment .reply .textBoxReply {
            /* float: right; */
            padding-top: 10px;
            margin-right: 80px;
            width: 60% !important;
            line-height: 25px;

        }




        .comments .comment .mainCmments .imgmain {
            width: 80px;
            height: auto;
            border-radius: 50%;
            overflow: hidden;
            /* vertical-align: middle; */
            margin-top: 15px;
            margin-bottom: 15px;
            float: right;
            margin-right: 15px;
            border: 1px solid #00000043;

        }

        .comments .comment .mainCmments .textBoxmain {
            /* float: right; */
            padding-top: 10px;
            margin-right: 105px;
            width: 85% !important;
            line-height: 25px;

        }

        .comments .comment .mainCmments .textBoxmain {
            /* float: right; */
            padding-top: 10px;
            margin-right: 105px;
            width: 85% !important;
            line-height: 25px;

        }

        .comments .comment .mainCmments .textBoxmain .borderx {

            border-left: 1px solid rgb(0, 0, 0);

        }






        .moreBtn1 {
            width: 80%;
            margin: 0 auto;
        }

        .moreBtn1 div {
            width: 50%;
            float: left;
            margin: 0px;
        }

        .moreBtn1 div p {
            font-size: 0.7rem;
        }

        .moreBtn1 div p span {
            padding-bottom: 3px;
        }

        .moreBtn1 div p svg {
            margin-left: 8px;
        }

        .clickedBookmarked {
            fill: rgb(255, 0, 0);
            stroke: black;
        }

        .clickedBookmarkedrep {
            fill: #FFD700;
            stroke: black;
        }
        #txtpad{
            padding-right: 3px;
        }
        @media (max-width: 768px) {
            .textmain{
            margin-left : 22%;
        }
        }
    </style>

</head>

<body>


    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <div class="uk-margin-small-top">
                        <div class="post">
                            <h5 class="uk-heading-line uk-text-right">
                                <span id="txtpad"> مطلب</span>
                            </h5>
                            <div class="uk-inline-clip uk-transition-toggle uk-light" tabindex="0">
                                {{--<img src="https://getuikit.com/docs/images/dark.jpg" alt="">--}}

                                <img src="/{!!$post->avatar_image_path!!}" alt="">

                                <div class="uk-position-center">
                                    <div class="uk-transition-slide-top-small text-center">
                                        <h4 class="uk-margin-remove"> {!!$post->title!!} </h4>
                                    </div>
                                    <br>
                                    <div class="uk-transition-slide-right-small text-center">
                                        <h4 class="uk-margin-remove">برای وبلاگ بیتکوین</h4>
                                    </div>
                                    <br>
                                    <div class="uk-transition-slide-bottom-small text-center">
                                        <h4 class="uk-margin-remove">برای وبلاگ بیتکوین</h4>
                                    </div>
                                </div>
                            </div>
                            <h3>{!!$post->title!!}</h3>
                            <p>{!!$post->content!!}</p>
                            <div id="moreBtn">
                                <div class="uk-align-left">
                                    <p class="text-right ">
                                        <img src="/img/svg/user.svg" width="20" height="20" uk-svg>
                                        <span class="uk-button-text">محمد علیپور</span>
                                    </p>

                                </div>
                                <div class="uk-align-center">

                                    <p class="text-center">
                                        <img src="/img/svg/clock.svg" width="20" height="20" uk-svg>
                                        <span class="uk-button-text">20 روز پیش</span>
                                    </p>
                                </div>
                                <div class="uk-align-right">

                                    <p class="text-left ">
                                        <img src="/img/svg/login.svg" width="20" height="20" uk-svg>
                                        <span class="uk-button-text">ورود به سایت</span>
                                    </p>

                                </div>
                            </div>
                            <br>
                            <br>



                            <h2 class="uk-text-primary uk-margin-medium-bottom">ارسال نظر</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="uk-margin">
                                        <div class="uk-form-controls">
                                            <input class="form-control noRadius  uk-form-large inputx" id="form-stacked-text"
                                                type="text" placeholder="نام">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="uk-margin">
                                        <div class="uk-form-controls">
                                            <input class="form-control noRadius  uk-form-large inputx" id="form-stacked-text"
                                                type="text" placeholder="ایمیل">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="uk-margin">
                                        <div class="uk-form-controls">
                                            <textarea class="form-control noRadius inputx" rows="5" placeholder="نظر خود را وارد کنید..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button class="uk-button uk-button-primary uk-button-large uk-align-center uk-width-1-1"
                                    id="btnx">
                                    <span class="">ثبت نظر</span>
                                </button>
                            </div>
                            <div class="comments">

                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c1">
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>ممد</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c2">
                                    <div class="reply">
                                        <a href="#c1" uk-scroll>
                                            <div class="imgReply">
                                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                                                    alt="">
                                            </div>
                                            <div class="textBoxReply">

                                                <p>ممد</p>
                                                <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده
                                                    می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                                                    کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                    چگونگی
                                                    نوع
                                                </p>

                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="https://is2-ssl.mzstatic.com/image/thumb/Purple30/v4/e1/27/30/e127305d-97be-eb78-f9c7-a3dc9ac061a9/source/512x512bb.jpg"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>مریم</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c3">
                                    <div class="reply">
                                        <a href="#c1" uk-scroll>
                                            <div class="imgReply">
                                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                                                    alt="">
                                            </div>
                                            <div class="textBoxReply">

                                                <p>ممد</p>
                                                <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده
                                                    می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                                                    کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                    چگونگی
                                                    نوع
                                                </p>

                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgQXhbivolCosFWGZcPVBJmOJZ5SHFxO9J-SpTEj04fbTJ1n8p"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>امیر</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c4">
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="https://store.playstation.com/store/api/chihiro/00_09_000/container/CA/en/999/UP1112-CUSA06917_00-AV00000000000016/1536293595000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>تیرکس</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c5">
                                    <div class="reply">
                                        <a href="#c1" uk-scroll>
                                            <div class="imgReply">
                                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0PDxAPDw8PDxUPEBAQDw0PDxAPDw8NFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAPFSsdFh0rKy0tLS0tLSstLS0tKystKysrKy0tLSstLSstKy0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABKEAABAwEBCAsOAgkFAAAAAAAAAQIRAwQFBhITITFxsRQyNFFSc4GRk7LRBxUWIjM1QVNUYXKSs8EjgiRCdIOUo9Li8BdiY6Lh/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECBAMFBv/EADQRAQABAwEFBwEIAgMBAAAAAAABAgMREgQxMjNRBRMhUnGBsTQUFUFhkaHh8CLBU2PRYv/aAAwDAQACEQMRAD8A0SDiuQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAKoCUwBEAIAmAIgCYAiAJgBAEQB6ss1RyS1qqi5lQiaogxKrYdXgOGqOqcSbDq8Bw1R1MSbDq8BxGqOppk2HV4DidUdTEmw6vAcRqjqaZNh1eA4nVHU0ybDq8BxGqOpiTYdXgOJ1R1NMmw6vAcNUdTTLxVIyL6MnKSggBAEQBMARAEwAgIVQEkAIAQAgBACAEAIAQAgDM3N8k383WU4V73SncuSqUgQBIEAAAACQNerJ4zvidrNEbnKVMEhACAEAIAQAgCqAEAIAQAgBACAEAIAroNRXtRcqK5EVPcRO5Mb2W2FS4Cc6nHVK+mHrTYjUwWpCJmQiZylUQAEgAIAASBAADwWx0ly4KZVnOuctqlGIWV0aDGYOCkThTn9xeiZnerVGFnB0VIAQAgBACAKoAQAgBACAEAIAQB7ULK58qiokZMqqVmqITEZe9Gwva5qqrcioudewrNcTCYpZI5LgEASAAgABIEAAJAAY66v6n5vsdLf4q1LGDqoQAgBACAEAVQQEAIAQAgBACAEAZC5iZHaU1HOteleHNYAAFASAkBICQEgJASAkABY3UTafm+x0t/irUsYOihACAEAIAQBVACAEAIAQAgBACAL+5uZ2lNRzrXpXZzWAJAyt7TUWvlSfEdrQ53eF1s8Ta8U3gpzIZ8tWDFN4KcyDJhOKbwU5kGTCMU3gpzIMmDFN4KcyDJgxTeCnMgyYMU3gpzIMmDFN4KcyDJhyy6Cfj1uOq9dT0qeGHm1cUreCypACAEAIAQBMAVQEkARACAJgCIAmAEAXlz8ztKHOtaldnNYAzlzbhtrUm1FqOTCwsiIkJCqn2OVVzE4w70WoqjOWTubcVtB+Gj1dkVsKiImWOw51XNUYdKLUUznLKnN1AAQBIAABAEuV3RT8etx1XrqenRwx6PMq4pW8FkJgCIAmAIgCYAQEKoISQAgBACAEAIAQBkLlWao9HKxjnQqIuCirlg5XJiN69FMzuhd2izVabHVKjHsaxFc57mqjWtTOqr6CkTE+ESvNMx4zDFd+rH7RR+dpfRV0U1R1bVcK+e5rLOxrrZZmqmFLVqtRdspnuWq5q4ZabdyiKfGWQ8LLl+22XpmdpXua/LLp3tHmg8LLl+3WbpmdpHc3PLJ3tHmhHhZcv22y9Mwnua/LJ3tHmhPhXcv26y9MztI7m55ZO9o80HhZcv22zdMztHc3PLJ3tHmgW+y5fttl6ZhPc1+WTvaPNB4WXL9tsvTM7R3Nflk72jzQeFly/bbL0zO0jua/LJ3tHmg8LLl+3Wbpmdo7m55ZO9o80Oc266tlWtVVK9JUWrUVFR6QqK5YU9CmmdMeDz6pjVLx752b11P50LaZ6IzB3zs3rqfzoNMmYO+dm9dT+dBpkzC4o1mPTCY5HJvtWUkhL0ggIAQSKoICAEAIAQAgBACANwvF8nW4xvVMm074a9m3SvL9fNts/Z6vVU52eZHq63eCXzseu8oAAAAAAAAAAAAAAAAbreruZvxP1nKvevTuZiCixACAKoAQAgBACAEAIAQBt943k63GN6pk2nfDXs26WWu/RbUstdj0lrqT2uSVSUVMqZDjbnFUS7XOGXMvBewep/mVf6jd3tXVg0Q2W414VyKtBj32aVXClcdXSYcqeh/uOFe0XInES0W7FE05mGE7o96VzrHYkrWahi345jcLG1X+KqOlIc5U9CHTZ71ddeKpUv2qKacxDlxuY2+0L3rErWqtLO1qr49TPHxGSblXVpi3T0V+Dlh9T/Mq/1Dvauqe7p6Le6NwLGyjVe2lCtpvc1cOosORqqnpJpuVTMeKtVumInwaOamd1i8C865lrufSr2ihhvc6qjn42s2UR6omRrkTMhgv3q6a5iJ8G2zZoqoiZjxUd0K9C5tjsDq1noYt6VKbUfjaz8irlyOcqE2L1dVeJnwL9qimjMQ5UbmJnKNz6Ktaqtzoi7Z2eNJTK2Ffe6jwP8As7tGowoq3Poo1yo3MiqnjO3tIzJhgy6rd71E/Rm/E/Wca969O5mIKrEAIAqgBACAEAIAQAgBAG3XkeTrcY3qmTad8NezbpZm6+56vwO1HCjih2r4ZaQa2JuN7u5qf5+spmucTZa4Yaz3X/NqftFLU47bJzHLauBxM9J57d6N9FkRrUXGZGoi+J6UTSZptVNEXaVfhVZP+T5P/SO5qT3tK3t98tlfRqsbhy+m9qS3JKtVE9JNNqqJiUTciYaaaWd3buV+aqHx1vqOPL2rmS9LZ+XDz7rPmt/G0dZOy8xG08tw09N5zNUrpUkaiLhZERM3uKYWyq760v8AdzDTJlTVunSVqp42VFTN7hhGWFLobxemn6K34n6zjXvXp3MzBVYgBAFcARAEwBEATACAIgCYA2y8lPw63GN6pk2nfDXs26WYuvuer8DtRwo4odq+GWjya2JuV7m5qf5+spmucTXa4Yaz3YPNqftFLU87bJzPZz2rgcTPSeeAAAADu3cq81UPjrfUceXtXMl6Wz8uHn3WvNb+No6ydl5iNp4HDT03nAAAAA3q9LcrfjfrONe9enczMFViAEAVwQEAIAQAgBACAEAVMe5u1c5vwuVNQxE705mF5cqq91ooo571RajUVFc5UVJ9KHO5EaZ8F7czqjxb3senwGfKhhzLfiOj0axESERE9yJBAprUWPSHta5M8ORHJPKTE4JjLx2BZ/U0ujZ2E6p6o0x0O99n9TS6NnYNU9TTHRPe+z+ppdGzsGqeppjojvfZ/U0ujZ2DVPU0x0NgWf1NLo2dg1T1NMdHvTpNYmC1qNTeaiIiciEZynCKtJr0hzWuTguRHJPKInBMPLvfZ/U0ujZ2E6p6o0x0c6ttmp42r4jPK1P1U4SnoUz/AIw8+qP8peOxqfAZ8qFsqmxqfAZ8qDIbGp8BnyoMitjETIiInuRIAqggIAQBVACAkgBACAEAIAQEEBK4uc7Br0nb1Rqxyla+GVqOKG5d814Cc5i0Nute2arhtR2aZycsFJjErxOYepCQAAAAAAAABzm3J+NV42p1lPSp4YebVxS8YJQQEEBJACAggJIAqgBACAEAIAQAgCnCTfI1R1dvs17/AI5/STCTfGqOp9mvf8dX6S9rE5MbT+NNZWqYxPimLF2JzNExHpLZDO6stYazEptRXNTPkVUnOpyqiculMxhcsqtXIjmroVFK4lbKshIEASAFAAUVarGJhPc1iZsJzkak6VJiJnciZiN7w742b19HpWdpbRV0lXXT1holrVFq1FRUVFqPVFTKiphLmN9O6GCrfLyglBACAEAIAQAgCqAEAIAQAgBABUCFqplfZ07oAl7WHytP426w47RyqvRtRD54Au7meU/Kv2KV7lqd7LnJ1AAAIAkAxF9O5l+Nms7WONxv8DToNrEQAgBACAEAIAQAgCqCAgkIICAEEjzrKqJkKVziPBu2CxReuTTXHhj/AHDxxinPXU9f7t2fy/vKko3bgCFqKzxm5FblRc+VC1EZqiJcNp5NfpKe/tq4afI02/Z6Oj5rVLY7j2h1Wix71lVwpWIzOVPsY7tMU1zEOkTmGQpVXMWW5Fzb5ymMrROHts6rvpzIRohOqTZ9XfTmQaINUmz6u+nMg0QapNn1d9OZBog1SbOq76cyDRBqljLddq0tqK1r0RERP1Gr6NB1otUzGZhxru1ROIlZWu6lorNwKjkVJRYRrUypoOlNummcw51XKqoxKyg6OZBAQSEEBBIQAggIJCCBXACAEAIAQB5V2KqZCtcZht2C/RZuTVXuxj4eOJcc+7l633ns/Wf0eZR6EAFNVstVE9KKWonFUTLlfomu3VTG+YWmxn+7nNv2ih4v3df6R+rO3LutRo0m03q7CbhTDVVMqqqZdCnCu3VcnVTulnrpm1Voq3wyVjurRrOwGKswq5WqiR/inKuzVRGZRFUSvjmkAAAAGBupWiq7Q3Uh2oqiIXp2G7djXTjHqtUrpvFtcJ+7L/SP1Tj2/wCIRrg+7No6R+r1TKXYaqZpmYnfCYCpACAEAIAQAgCqAEAIAQAgCFAhVQCwUzPsqd0ASAALC0bdeTUh6Fjlw+d276ir2+IZO9ny/wC7drQrtPB7s1O9tkmB0AAAAoGtXV8s7k1IS9zYeTHv8rQNYBfU1SE0GiNz5G9zKvWflUS5JgBACAEAIAQBVACAEAIAQBbWz0FK9z0uzKaarsxVGfD/AHC1k5Znq93uLXkj9ICHQAAAKVYm8i8hMVTG6VJt0TOZpiZ9HnXVWJLVVqzEt8VY0od7E6qsT4sHaFumm1mmIjxeC2ur62p87u02aKekPEy6tY6TVp05a3aNlYTKsIeJVM5lriIw9sSzgt5kIzKcQjEs4LeZBmTEJxLOC3mQZkxDQ75WolqqIiRtcifCh2p3PY2PlR7sYWaQBJOZc+5tzvoj9IXFkzqdLcvI7Vopp0aYiN/+l3B0eOQAgBACAEAVQAgBACALSpaVRVSMxzmvE4w9Sx2b3tuK9eM/l/Lxq1ldnK1V5hv2TYe4rmrVnwxux/t5lHoAAC4ubZcfWpUcLBxr2sw4wsGfTHpJpjM4c7tzu6JrxnDb/wDT1fa0/h/7zv3H5vL+9v8Ar/f+Gq3aufsW0VKGHjMXg+Pg4M4TGuzSsbaM5xrp0zh6Wz3e9txXjGf/AFi7ZteVDrs/Gzdpcn3haKb3gtto37K1rW7H2qIk43PCRwTBOxZnOr9v5dYu/kz17t3FtiVFxeLxatTb4czPuTeM1+z3WPHOXSivUyVvtGKpVKsYWLY9+DMTgoqxPozHKinVVEdV5nEZal4dL7N/O/sN32H/AOv2/lx778mIttv2TUdWwcDCjxZwohIzwm8cqrfdzpzl7mwzmzE+vyurgXK2ZXShh4uWOdh4OHtYyRKCinVOF9pv9zb14y2b/T1fa0/h/wC87dx+bz/vb/r/AH/hptqo4upUpzOLqPZhRE4LlbMejMcJjE4etRVqpirrESppVFauQmmrDLtmx/aNP+WMZ/DO/D2S1LvIXiv8mCvsrTTNXebo6fyvEOjxyAEAIAQBVACAEAIAxdbbLpOFe99RsP09H9/GVBVrAAADI3t7tsvHM1l7fFDPtfIr9HX1Nr5Zyq/XzhaNNL6TDHd45fS7B9PT7/Mtdte15UL7Pxqdpcn3haG54IBt94dpp0218N7GSrIwnNbORd8w7ZTMzGIdrUxGWeuzb6DrNXRK1JVWjVRESo1VVVauRMpmtUVRXT4Tvh0qmMT4uZHrsq8su0TSuswX+OX0HZ/Ij3+W03gbubxVX7EWeJXtPke8OnIa3zri91d0Wjj63XcYKt8vrbHLp9I+FsQ6JbnTSTG9zvcur0n4ZVqZDQ+QTACAEAICVcEIIAQBEAYmvtnaTjXvfUbB9PR/fxlQVawAAAyN7m7bNxzNZe3xQz7XyK/R182vlnKr9POFo00vpMMd3jl9L2f9PT7/ADLXbZteVC+z8anaXJ94WhueCAAIAkC8sm0TSuswX+OX0HZ/Ij3+W0XgbubxVX7EWeJXtPke8Onoa3zri91d0Wjj63XcYat8vrbPKp9I+FsVdEtzppQmN7ne5dXpPwzDUyHd8gmAEAIAQBVASQAgBAGHtG3dpONe99PsP09H9/GXmVawAAAyN7e7bNxzNZe3xQz7XyK/R182vlnKr9fOFo00vpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpxrfOuMXU3RaOPrddxhq3y+ts8qn0j4WpV0S3OmlCY3ud7l1ek/DNNTId3yKYAQAgBAFUAIAQAgDC2nbu0nGve+n2H6ej+/jLzKtYAAAZG9vdtl45msvb4oZ9r5Ffo7AbXyzlN+vnC0fuvpMMd3jl9LsH09Pv8y122bXlQvs/Gp2lyfeFobnggAAAAvLLtE0rrMF/jl9B2fyI9/ltN4G7m8VV+xFniV7T5HvDpyGt864xdTdFo4+t13GCrfL62xyqfSPhakOiWZ00oTG9zvcur0n4ZxqZDu+RTACAEAIAqgBACAAGEtO3dpONe99PsP09H9/GXkVawAAAyN7e7bLxzNZe3xQz7XyK/R19Ta+Wcqv184Wj919Jhju8cvpdg+np9/mWu2za8qF9n41O0uT7wtDc8EAAAAF5ZNomldZgv8AHL6Ds/kR7/LabwN3N4qr9iLPEr2nyPeHTkNb51xe6u6bRx9bruMNW+X1tjl0+kfC2KuiWZ00prJje53uXV6T8M81Mh3fIpgBACAEASAAAAMHa/KO0/Y41b30+w/T0f38ZeRVrAIAkDI3ubts3HM1l7fFDPtfIr9HYFNr5Zyi/TzhaNNL6TDHd45fS7B9PT7/ADLXrZteVC+zcanaXJ94WhueCgCQAAC8su1TlMF/jl9B2fyI9/ltN4G7m8VU+xFniV7T5HvDpyZ0Nb51xe6m6LRx9bruMNXFL6yzy6fSPhbFXVVT2yaU1kxvc73Lq9J+GfbmO75EAAAAH//Z"
                                                    alt="">
                                            </div>
                                            <div class="textBoxReply">

                                                <p>ممد</p>
                                                <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده
                                                    می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                                                    کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                    چگونگی
                                                    نوع
                                                </p>

                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9N12agWUTFIhrCo2JjILkuWIq-bH5cHRyOGh_5h85KnQq2lCaTg"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>ماریو</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="comment uk-margin-medium-bottom uk-background-muted" id="c6">
                                    <div class="reply">
                                        <a href="#c2" uk-scroll>
                                            <div class="imgReply">
                                                <img src="https://is2-ssl.mzstatic.com/image/thumb/Purple30/v4/e1/27/30/e127305d-97be-eb78-f9c7-a3dc9ac061a9/source/512x512bb.jpg"
                                                    alt="">
                                            </div>
                                            <div class="textBoxReply">

                                                <p>مریم</p>
                                                <p class="textReply uk-text-truncate">کلی طرح سفارش گرفته شده استفاده
                                                    می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع
                                                    کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                    چگونگی
                                                    نوع
                                                </p>

                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="mainCmments">
                                        <div class="imgmain">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfh91754-ZayjDY344zReUn_U3PHUsJ5zkrb8ABEt3flq9Lxpr"
                                                alt="">
                                        </div>
                                        <div class="textBoxmain">

                                            <h4>احمد</h4>
                                            <span class="uk-margin-medium-left uk-button-text">2 روز پیش</span>
                                            <span class="borderx"></span>
                                            <span class="uk-margin-medium-right uk-button-text uk-text-primary">پاسخ</span>
                                            <p class="textmain">کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر
                                                گرافیکی نشانگر چگونگی نوع
                                                کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر
                                                چگونگی
                                                نوع
                                            </p>

                                        </div>
                                        <div class="moreBtn1">
                                            <div class="uk-align-left repbox">
                                                <p class="text-right ">
                                                    <span class="repbtn" uk-icon="icon: warning"></span>
                                                    <span class="uk-button-text reptxt">گزارش تخلف</span>
                                                </p>
                                            </div>
                                            <div class="uk-align-right">
                                                <p class="text-left likebox">
                                                    <span class="likebtn" uk-icon="icon: heart"></span>
                                                    <span class="uk-button-text liketxt">522</span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 uk-background-muted">
                    <div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>
                        <div class="morePost uk-padding-small uk-margin-bottom">
                            <div class="uk-inline-clip uk-transition-toggle" tabindex="0">
                                <img class="uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/photo.jpg"
                                    alt="">
                            </div>
                            <h4>تست پست</h4>
                            <p class="uk-text-small uk-text-primary">وعی وابسته به متن می‌باشد آنها با استفاده از
                                محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند
                            </p>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </main>





    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <!-- script -->
    <script>
        $(document).ready(function () {
            $(".likebox").click(function () {
                $(this).find(".likebtn svg path").toggleClass("clickedBookmarked");
                $(this).find(".liketxt").toggleClass("uk-text-primary");
                // alert("ttt");
            });
            $(".repbox").click(function () {
                $(this).find(".repbtn svg circle:nth-child(1)").attr("fill", "black");
                $(this).find(".repbtn svg circle:nth-child(2)").toggleClass("clickedBookmarkedrep");
                $(this).find(".reptxt").toggleClass("uk-text-primary");
                // UIkit.notification({message: 'Bottom Center...', pos: 'bottom-center'});
                UIkit.notification({ message: '<span uk-icon=\'icon: check\'></span> گزارش شما با موفقیت ارسال شد', pos: 'bottom-left', status: 'primary' });
                // alert("ttt");
            });
            $(".reply").click(function () {
                var a = $(this).find("a").attr("href");
                $(a).css("box-shadow", "0px 5px 5px #00000062");
            });
        });


    </script>
</body>

</html>