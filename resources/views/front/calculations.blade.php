@extends('layouts.ProfileMasterPage') @section('SubCss')
<title>بیتکوین - محاسبه</title>
<style>
    .fullBox{
            height: auto;
            /* border:1px solid gray; */
            background-color: #ebebeba1;
            -webkit-box-shadow: -1px 1px 14px 3px rgba(0,0,0,0.17);
-moz-box-shadow: -1px 1px 14px 3px rgba(0,0,0,0.17);
box-shadow: -1px 1px 14px 3px rgba(0,0,0,0.17);
        }
        /* style of option */


/*Styling Selectbox*/
.dropdown {
  width: 250px;
  /* display: inline-block; */
  background-color: #dcdde1;
  border-bottom-right-radius : 3px;
  border-bottom-left-radius : 3px;
  box-shadow: 0 0 2px rgb(204, 204, 204);
  transition: all .5s ease;
  /* position: relative; */
  font-size: 14px;
  color: #474747;
  height: auto;
  text-align: center;
  border: 1px solid rgba(128, 128, 128, 0.415);
  border-top: none;
  margin: 0 auto;
}
.dropdown span {
    font-size: 16px;
}
.dropdown .select {
    cursor: pointer;
    /* display: block; */
    padding: 10px
}
.dropdown .select > i {
    font-size: 15px;
    color: #888;
    cursor: pointer;
    transition: all .3s ease-in-out;
    /* float: right; */
    line-height: 25px;
    /* margin-bottom: 10px; */
}
.dropdown:hover {
    box-shadow: 0 0 4px rgb(204, 204, 204)
}
.dropdown:active {
    background-color: #f8f8f8
}
.dropdown.active:hover,
.dropdown.active {
    box-shadow: 0 0 4px rgb(204, 204, 204);
    border-radius: 5px 5px 0 0;
    background-color: #f8f8f8
}
.dropdown.active .select > i {
    transform: rotate(-90deg)
}
.dropdown .dropdown-menu {
    position: absolute;
    background-color: #fff;
    width: 100%;
    /* height: 100%; */
    left: 0;
    margin-top: 1px;
    box-shadow: 0 1px 2px rgb(204, 204, 204);
    border-radius: 0 1px 5px 5px;
    overflow: hidden;
    /* display: none; */
    max-height: 220px;
    /* overflow-y: auto; */
    z-index: 9
}
.dropdown .dropdown-menu li {
    padding: 10px;
    transition: all .2s ease-in-out;
    cursor: pointer
} 
.dropdown .dropdown-menu {
    padding: 0;
    list-style: none
}
.dropdown .dropdown-menu li:hover {
    background-color: #f2f2f2
}
.dropdown .dropdown-menu li:active {
    background-color: #e2e2e2
}
.text-orange{
    color : #feca57;
}
        /* style of option */      
        @media (max-width: 768px) {
            .textMobile {
                float: none!important;
                margin-right: 10px !important;
                text-align: center; 
            }
            .btn-3{
                margin-left: auto !important;   
                margin-right: auto !important;
                float: none;
            }
            #btn12{
                margin-left: auto !important;   
                margin-right: auto !important;
                float: none;
            }
            .forInput input{
                width: 90%;
                height: 30%;
                border-color: rgba(136, 136, 136, 0.612);
                margin: 0 auto;
            }
            .centerOnMobile{
                text-align: center;
            }
        }        
        .forInput input{
                height: 30%;
            }
            .fontMedium{
                font-size: 20px;
            }
            .btnShadow{
                -webkit-box-shadow: 1px 3px 22px 6px rgba(184,242,204,1);
-moz-box-shadow: 1px 3px 22px 6px rgba(184,242,204,1);
box-shadow: 1px 3px 22px 6px rgba(184,242,204,1);5,1);
            }
            .textGold{
                color: #ffc107 !important;
            }
</style>
@endsection
@section('SubMain')
<br>
<div class="container">
    <div class="fullBox uk-margin-medium-top uk-margin-medium-bottom">

        <div class="dropdown">
            <div class="select">
                <span>ارز مورد نظر را انتخاب کنید</span>
                <i class="fa fa-chevron-left"></i>
            </div>
            <ul class="dropdown-menu">
                <li id="bitCoin" class="text-center">بیتکوین</li>
                <li id="liteCoin" class="text-center">لایت کوین</li>
                <li id="Ether" class="text-center">اتریوم</li>
                <li id="halfCash" class="text-center">هاف کش</li>
                <li id="dashCoin" class="text-center">دش کوین</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 col-12">
                <h4 class="uk-margin-medium-top uk-margin-small-right centerOnMobile ">محاسبه هر بیتکوین بر
                    اساس:</h4>
            </div>

            <div class="col-md-3 col-6">
                <p class="uk-margin-medium-top uk-align-left textMobile"><span class="fontMedium text-orange ">۱۰۹,۳۵۰,۲۶۴</span>
                    ریال</p>
            </div>
            <div class="col-md-3 col-6">
                <p class="uk-margin-medium-top uk-align-left textMobile uk-margin-left"><span class="fontMedium text-orange">۶۵۴,۷۹۲</span>
                    دلار</p>
            </div>
            <div class="col-md-3">
                <h4 class="uk-margin-top uk-margin-small-right centerOnMobile">انتخاب دستگاه:</h4>
            </div>
            <div class="col-md-3 col-6">
                <button class="btn-3 btn btn-outline-primary uk-align-left uk-margin-top">
                    ANTMINE S8
                </button>
            </div>
            <div class="col-md-3 col-6">
                <button class="btn-3 btn btn-outline-primary uk-align-left uk-margin-top">
                    ANTMINE S8
                </button>
            </div>
            <div class="col-md-3 col-12">
                <button id="btn12" class="btn btn-outline-primary uk-align-left uk-margin-top uk-margin-left">
                    ANTMINE S8
                </button>
            </div>


            <div class="col-md-3">
                <h4 class="uk-margin-top uk-margin-small-right centerOnMobile">تعرفه برق:</h4>
            </div>
            <div class="col-md-3 col-6">
                <button class="btn-3 btn btn-outline-dark uk-align-left uk-margin-top">
                    خانگی
                </button>
            </div>
            <div class="col-md-3 col-6">
                <button class="btn-3 btn btn-outline-dark uk-align-left uk-margin-top">
                    صنعتی
                </button>
            </div>
            <div class="col-md-3 col-12">
                <button id="btn12" class="btn btn-outline-dark uk-align-left uk-margin-top uk-margin-left">
                    کشاورزی
                </button>
            </div>

            <div class="col-md-4 col-12 forInput">
                <h5 class="uk-margin-right uk-margin-medium-top">مصرف هر کیلووات:</h5>
                <input type="text" class="form-control uk-width-3-4 text-center uk-margin-right ">
            </div>
            <div class="col-md-4 col-12 forInput ">
                <h5 class="uk-margin-right uk-margin-medium-top">قدرت پردازش:</h5>
                <input type="text" class="form-control uk-width-3-4 text-center uk-margin-right ">
            </div>
            <div class="col-md-4 col-12 forInput">
                <h5 class="uk-margin-right uk-margin-medium-top">هش/ریت:</h5>
                <input type="text" class="form-control uk-width-3-4 text-center uk-margin-right">
            </div>
            <div class="col-md-12">
                <button class="btn btn-success btnShadow uk-align-center uk-width-2-3 uk-margin-medium-top">
                    <span>محاسبه</span>
                </button>
                <br>
            </div>
            <div class="col-md-12">
                <table class="uk-table uk-table-divider uk-table-hover">
                    <caption></caption>
                    <tr>
                        <td></td>
                        <td class="text-center textGold">بیتکوین</td>
                        <td class="text-center">مصرف برق</td>
                        <td class="text-center">سود(تومان)</td>
                    </tr>
                    <tr class="text-center">
                        <td>روزانه</td>
                        <td>۰</td>
                        <td>۰</td>
                        <td>۰</td>
                    </tr>
                    <tr class="text-center">
                        <td>هفتگی</td>
                        <td>۰</td>
                        <td>۰</td>
                        <td>۰</td>
                    </tr>
                    <tr class="text-center">
                        <td>ماهانه</td>
                        <td>۰</td>
                        <td>۰</td>
                        <td>۰</td>
                    </tr>
                    <tr class="text-center">
                        <td>سالانه</td>
                        <td>۰</td>
                        <td>۰</td>
                        <td>۰</td>
                    </tr>

                </table>
            </div>
        </div>
        <br>
    </div>
</div>
@endsection

@section('SubScript')
<script>
$('.dropdown').click(function () {
$(this).attr('tabindex', 1).focus();
$(this).toggleClass('active');
$(this).find('.dropdown-menu').slideToggle(300);
});
$('.dropdown').focusout(function () {
$(this).removeClass('active');
$(this).find('.dropdown-menu').slideUp(300);
});
$('.dropdown .dropdown-menu li').click(function () {
$(this).parents('.dropdown').find('span').text($(this).text());
$(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
});
/*End Dropdown Menu*/


$('.dropdown-menu li').click(function () {
var input = '<strong>' + $(this).parents('.dropdown').find('input').val() + '</strong>',
msg = '<span class="msg">Hidden input value: ';
    $('.msg').html(msg + input + '</span>');
});

</script>
@endsection