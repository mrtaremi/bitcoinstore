@extends('layouts.MasterPage') @section('SubCss')
<title>بیتکوین - ورود و عضویت1</title>
<style>
  #Box1 {
    box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5)
  }

  #headBox {
    background-image: linear-gradient(145deg, #33CCFF 30%, #c2e59C 100%) !important;
    width: 100%;
    height: 120px;
  }

  .titleBox {
    text-shadow: 2px 2px 2px rgb(140, 140, 140);
  }

  #headBox h2 {
    line-height: 120px;
  }

  #Box1 #rightBox {
    border-left: 1px solid gray;
  }

  .successColorText {
    color: green;
  }

  .errorColorText {
    color: red;
  }

  .pMargin {
    margin-top: 5px !important;
    margin-inline-start: 5px !important;
  }

  .spanBg {
    background-color: #1e87f0;
    color: white;
  }

  .uk-input::placeholder {
    color: rgba(38, 152, 251, 0.592);
    padding-right: 10px;

    /* Firefox */
  }

  :-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: rgba(38, 152, 251, 0.592);
    padding-right: 10px;
  }

  ::-ms-input-placeholder {
    /* Microsoft Edge */
    color: rgba(38, 152, 251, 0.592);
    padding-right: 10px;
  }

  .inputBox {
    padding-left: 30px;
    padding-right: 30px;
  }

  #btnBox {
    width: 85%;
  }

  .disableBox {
    filter: blur(5px) grayscale(100%);
    transition: 1000ms;
  }

  .btn-login {
    color: #fff;
    background-color: #007bff;
  }

  .btn-loginG {
    color: #fff;
    background-color: #28a745;
  }

  .btn-loginF {
    color: #fff;
    background-color: #3b5998;
  }

  hr {
    /* margin-top: 10px;
              margin-bottom: 0px;
              clear: both;
              border: 0;
              height: 1px;
              background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
              background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
              background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
              background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0)); */
  }

  /* .active{
    filter: blur(5px) grayscale(100%);
  } */

  .tabx {
    width: 100%;
    height: 26px;
    /* background-color: #33CCFF; */
    border-bottom: 1px solid rgba(0, 0, 0, 0.16);
    padding: 0px;
    margin: 0px;
  }

  .tabx li {
    float: right;
    width: 50%;
    text-align: center;
    list-style-type: none;
    text-decoration: none;
    transition: 500ms;
    /* border-bottom: 3px solid #33CCFF; */
  }

  .tabx .ac {
    border-bottom: 2px solid #33CCFF;
    transition: 500ms;
  }

  .tabx li a {
    text-decoration: none;
    color: #000;
  }

  .none {
    display: none;
  }
</style>
@endsection @section('SubModal')
<div id="modal-overflow" class="uk-margin-medium-top" uk-modal>
  <div class="uk-modal-dialog">

    <button class="uk-modal-close-default" type="button" uk-close></button>

    <div class="uk-modal-header uk-margin-small-right">
      <h4 class="uk-modal-title">تایتل</h4>
    </div>

    <div class="uk-modal-body" uk-overflow-auto>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
      <p>طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن
        می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
        پایان برند.</p>
    </div>

    <div class="uk-modal-footer uk-text-right">
      <button class="uk-button uk-button-primary" type="button">قبول دارم</button>
      <button class="uk-button uk-button-default uk-modal-close" type="button">انصراف</button>
    </div>

  </div>
</div>
@endsection @section('SubMain')

<main class="mobile uk-margin-top">
  <br>
  <br>
  <br>
  <div class="container">
    <div>
      <ul class="tabx">
        <li id="signuph" class="ac">
          <a href="#">عضویت</a>
        </li>
        <li id="signinh">
          <a href="#">ورود</a>
        </li>
      </ul>
    </div>
    <div class="pages">
      <div id="signupb" class="page">
        <br>

        <form method="post" action="{{ url('/register') }}">

          {!! csrf_field() !!}

          <div class="" id="">

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-inline">
                <span class="spanBg uk-form-icon uk-form-icon-flip">
                  <img src="/img/svg/email.svg" width="20" height="20" uk-svg>
                </span>
                <input class="uk-input uk-form-width-large " name="email" type="email" placeholder="ایمیل" required>

              </div>
              <p class="pMargin successColorText">error</p>
            </div>

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-inline">
                <span class="spanBg uk-form-icon uk-form-icon-flip">
                  <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                </span>
                <input name="password_confirmation" class="uk-input  uk-form-width-large " type="password" placeholder="رمز عبور" required>
              </div>
              <p class="pMargin successColorText">error</p>
            </div>

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-inline">
                <span class="spanBg uk-form-icon uk-form-icon-flip">
                  <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                </span>
                <input class="uk-input  uk-form-width-large " type="password" placeholder="تکرار رمز عبور" required>
              </div>
              <p class="pMargin successColorText">error</p>
            </div>

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-form-controls">
                <label for="radio1">
                  <input class="uk-radio uk-margin-small-left" type="radio" name="radio1" required>با
                  <a href="#modal-overflow" uk-toggle>قوانین</a> موافقم</label>
              </div>
              <p class="pMargin successColorText">error</p>
            </div>

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-align-center" id="btnBox">
                <input type="submit" name="login-submit" id="" tabindex="4" class="form-control btn btn-login fontRegular uk-margin-bottom"
                  value="عضویت">
                <input type="submit" name="login-submitG" id="login-submit" tabindex="4" class="form-control btn btn-loginG fontRegular uk-margin-bottom"
                  value="ورود با گوکل">
                <input type="submit" name="login-submitF" id="login-submit" tabindex="4" class="form-control btn btn-loginF fontRegular uk-margin-bottom"
                  value="ورود با فیسبوک">
              </div>
            </div>

          </div>

        </form>

      </div>
      <div id="singinb" class="page">
        <br>
        <div class="" id="">
          <form method="post" action="{{ url('/login') }}">

            {!! csrf_field() !!}

            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-inline">
                <span class="spanBg uk-form-icon uk-form-icon-flip">
                  <img src="../img/svg/user.svg" width="20" height="20" uk-svg>
                </span>
                <input class="uk-input uk-form-danger uk-form-width-large " name="email" type="text" placeholder="ایمیل">
              </div>
              <p class="pMargin successColorText">error</p>
            </div>
            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <div class="uk-inline">
                <span class="spanBg uk-form-icon uk-form-icon-flip">
                  <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                </span>
                <input class="uk-input uk-form-success uk-form-width-large " name="password" type="password" placeholder="رمز عبور">
              </div>
              <p class="pMargin successColorText">error</p>
            </div>
            <div class="uk-margin inputBox uk-margin-medium-bottom">
              <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login fontRegular uk-margin-bottom"
                value="ورود">
              </button>
            </div>


          </form>
        </div>
      </div>
    </div>

  </div>
</main>



<main class="desktop uk-margin-medium-top">


  <div class="container ">


    <div>
      <div class="uk-padding">
        <div id="Box1" class="row  uk-background-muted">
          <div id="headBox" class=" uk-margin-medium-bottom">
            <h2 class="text-white text-center align-middle titleBox">ورود / عضویت</h2>
          </div>
          <div class="col-md-6 uk-margin-bottom disableBox" id="rightBox">
            <h4 class="uk-text-meta">ورود</h4>

            <form method="post" action="{{ url('/login') }}">

              {!! csrf_field() !!}

              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-inline">
                  <span class="spanBg uk-form-icon uk-form-icon-flip">
                    <img src="../img/svg/user.svg" width="20" height="20" uk-svg>
                  </span>
                  <input class="uk-input uk-form-width-large " name="email" type="text" placeholder="ایمیل" required>
                </div>
              </div>

              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-inline">
                  <span class="spanBg uk-form-icon uk-form-icon-flip">
                    <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                  </span>
                  <input class="uk-input uk-form-width-large " name="password" type="password" placeholder="رمز عبور " required>
                </div>

              </div>
              <p class="text-center pMargin errorColorText"></p>
              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-align-center" id="btnBox">
                  <input type="submit" name="login-submit" id="" tabindex="4" class="form-control btn btn-login uk-margin-bottom" value="ورود"
                    required>
                </div>
              </div>

            </form>

          </div>
          <div class="col-md-6 " id="leftBox">
            <h4 class="uk-text-meta">عضویت</h4>
            <form method="post" action="{{ url('/register') }}">

              {!! csrf_field() !!}

              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-inline">
                  <span class="spanBg uk-form-icon uk-form-icon-flip">
                    <img src="../img/svg/email.svg" width="20" height="20" uk-svg>
                  </span>
                  <input class="uk-input uk-form-width-large" id="" name="email" type="email" placeholder="ایمیل" required>
                </div>
                <p class="pMargin successColorText"></p>
              </div>

              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-inline">
                  <span class="spanBg uk-form-icon uk-form-icon-flip">
                    <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                  </span>
                  <input name="password" class="uk-input  uk-form-width-large " type="password" placeholder="رمز عبور" required>
                </div>
                <p class="pMargin successColorText"></p>
              </div>
              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-inline">
                  <span class="spanBg uk-form-icon uk-form-icon-flip">
                    <img src="../img/svg/lock.svg" width="20" height="20" uk-svg>
                  </span>
                  <input name="password_confirmation" class="uk-input  uk-form-width-large " type="password" placeholder="تکرار رمز عبور" required>
                </div>
                <p class="pMargin successColorText"></p>
              </div>
              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-form-controls">
                  <label for="radio1">
                    <input class="uk-radio uk-margin-small-left" type="radio" name="radio1" required>با
                    <a href="#modal-overflow" uk-toggle>قوانین</a> موافقم</label>
                </div>
                <p class="pMargin successColorText">error</p>
              </div>




              <div class="uk-margin inputBox uk-margin-medium-bottom">
                <div class="uk-align-center" id="btnBox">
                  <input type="submit" name="login-submit" id="" tabindex="4" class="form-control btn btn-login fontRegular uk-margin-bottom"
                    value="عضویت">
                  <input type="submit" name="login-submitG" id="login-submit" tabindex="4" class="form-control btn btn-loginG fontRegular uk-margin-bottom"
                    value="ورود با گوکل">
                  <input type="submit" name="login-submitF" id="login-submit" tabindex="4" class="form-control btn btn-loginF fontRegular uk-margin-bottom"
                    value="ورود با فیسبوک">
                </div>
              </div>

            </form>


          </div>
        </div>
      </div>

    </div>

</main>
@endsection @section('SubScript')
<script>
  $(function () {

    //  alert("rrr");

    $('#login-form-link').click(function (e) {
      $("#login-form").fadeIn(100);
      $("#register-form").fadeOut(100);
      $('#register-form-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });


    $('#register-form-link').click(function (e) {
      $("#register-form").fadeIn(100);
      $("#login-form").fadeOut(100);
      $('#login-form-link').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });





    $("#rightBox").click(function () {
      $("#rightBox").removeClass("disableBox");
      $("#leftBox").addClass("disableBox");



      // $("#rightBox form").removeClass("disableBox");
      // $("#leftBox form").addClass("disableBox");
      // $("#rightBox form input").removeAttr("disabled", "disabled");
      // $("#leftBox form input").attr("disabled", "disabled");
    });

    $("#leftBox").click(function () {
      $("#leftBox").removeClass("disableBox");
      $("#rightBox").addClass("disableBox");


      // $("#leftBox form").removeClass("disableBox");
      // $("#rightBox form").addClass("disableBox");
      // $("#leftBox form input").removeAttr("disabled", "disabled");
      // $("#rightBox form input").attr("disabled", "disabled");

    });


    if ($(window).width() < 768) {
      $(".desktop").hide();
    }
    if ($(window).width() >= 768) {
      $(".mobile").hide();
    }


    $(".tabx li").click(function () {

      $(".tabx li").removeClass("ac");
      $(this).addClass("ac");
    });


    // $(".pages #signinb").hide();




    $(".tabx #signinh").click(function () {
      $(".pages #signupb").hide();
      $(".pages #signinb").fadeIn(1000);
    });


    $(".tabx #signuph").click(function () {
      $(".pages #signinb").hide();
      $(".pages #signupb").fadeIn(1000);
    });

  });


</script> @endsection