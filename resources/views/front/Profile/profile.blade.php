@extends('layouts.ProfileMasterPage') @section('SubCssProfile')
<title>بیتکوین - اصلی</title>
<style>

    .boxProf2 {
        width: 100%;
        height: auto;
        border: 1px solid;
        padding: 10px;
        box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
        padding: 0 !important;
        margin: 0 auto;
        border-radius: 5px;
    }

    .boxProfBookmark {
        width: 100%;
        min-height: 322px;
        max-height: 322px;
        border: 1px solid;
        padding: 10px;
        box-shadow: 1px 1px 5px rgba(128, 128, 128, 0.5);
        padding: 0 !important;
        margin: 0 auto;
        overflow: scroll;
        overflow-x: hidden;
        border-radius: 5px;
    }

    tr:hover {
        /* padding : 7px !important; */
        /* background-color: red !important; */
    }

    .border11 {
        border: 1px solid gray;
    }

    .prof1 {
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 15px;
        padding-right: 15px;
    }

    .colorGray {
        color: gray !important;
    }

    .userName1 {
        margin-right: 5px;
        margin-top: -5px;
    }

    .paddingCustom {
        padding-top: 5px;
    }

    #editProf {
        padding: 0px !important;
    }

    .editProfText {
        border-top: 1px solid gray;
        vertical-align: middle;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 0px;
        padding-right: 0px;
        margin: 0px;
        color: #2799FB !important;
        cursor: pointer;

    }

    .imgFav {
        width: 92px;
        height: 90px;
        margin-left: 5px;
    }

    .viewAllPrc {
        padding-top: 2px;
        padding-bottom: 10px;
        padding-left: 0px;
        padding-right: 0px;
        margin-bottom: 0px;
        margin-top: 10px;
        color: #2799FB !important;
        cursor: pointer;
    }

    .deleteBtn {
        cursor: pointer;
    }

    .boxProf3 {
        width: 100%;
        height: auto;
        border: 1px solid;
        padding: 10px;
        padding: 0 !important;
        margin: 0 !important;
        margin-top: 20px !important;
    }

    .boxProf4 {
        width: 100%;
        height: auto;
        border: 1px solid;
        padding: 10px;
        padding: 10px;
        margin: 0 !important;

    }

    #noBorder {
        border-bottom: 0px !important;
        border-right: 0px !important;
        border-left: 0px !important;

    }

    .boxProf4 .row h5 {
        float: right;
        margin-top: 3px;
        margin-left: 0;
        margin-bottom: 0;
        margin-right: 20px;
        cursor: pointer;
    }

    .boxProf4 .row span {
        float: right;
        margin-top: 5px;
        margin-right: 30px;
    }

    .activeClc {
        background-color: rgba(123, 152, 210, 0.427);
        padding: 0;
        margin: 0;
        width: 100%;
    }
    table{
        border-top-right-radius: 5px;
    }
    #tableHover tr:hover {

        background-color: rgba(51, 204, 255, 0.163) !important;
    }

    #tableHover #thBgcolor {

        background-color: rgba(72, 94, 202, 0.653) !important;
    }

    #tableHover th {

        color: white;
    }

    #tableHover .successColor {
        color: rgb(33, 171, 33) !important;
    }

    #tableHover .unsuccessColor {
        color: rgba(255, 0, 0, 0.796) !important;
    }

    .uk-modal-title1 {
        font-size: 1.5rem !important;
        line-height: 1.3 !important;
        padding-right: 15px !important;
    }


    .borderButton {
        border-left: 1px solid gray;
        line-height: 30px;
        background-color: rgba(255, 255, 255, 0.932);
    }

    .h5Margin {
        margin-bottom: 7px;
    }

    #nameBorder {
        border-left: 1px solid gray;
        border-bottom: 1px solid gray;
    }

    #mailBorder {
        border-bottom: 1px solid gray;
    }

    #phoneBorder {
        border-left: 1px solid gray;
        border-bottom: 1px solid gray;
    }

    #codeBorder {
        border-bottom: 1px solid gray;
    }

    #notifBorder {
        border-left: 1px solid gray;
    }

    #cardStyle {
        text-align: right;
    }

    #bordeDown {
        border-bottom: 1px solid gray;
    }

    #floatRight {
        float: right;
    }

    #blockText {
        display: inline-block
    }

    #delPrc {
        float: left !important;
        display: inline;
        color: #FF3A3A;
        margin-top: 10px;
    }

    #priceStyles {
        margin-top: 0px;
        line-height: 0px;


    }

    .imgDanger {
        width: 90px;
        height: auto;
        display: block;
        margin: 0 auto;
        margin-top: 20px !important;

    }

    .bg-span {
        background-color: #1e87f0;
        color: #fff;
    }

    .imgPrcForDelete {
        width: 92px;
        height: 89px;
        display: block;

    }

    .boxForImgAndText {
        width: 80%;
        height: 92px;
        margin: 0 auto;
        /* border : 1px solid #ED4C67; */
        border-radius: 2px;
    }

    .boxForImgAndText p {
        line-height: 41px;
        margin-right: 20px;
    }

    .boxForImgAndText img,
    .boxForImgAndText p {
        float: right;
    }

</style>
@endsection @section('SubModalProfile')
<div id="Change-inforamtion-Modal" class="uk-margin-medium-top" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="reset" uk-close></button>
        <div class="uk-modal-header">
            <p class="uk-modal-title1">ویرایش اطلاعات</p>
        </div>
        <form>
            <div class="uk-modal-body">
                <div class="row">
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class=""> نام: </label>
                            <input maxlength="25" class="form-control noRadius" type="text" placeholder="محمد">
                        </div>
                    </div>
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class="">نام خانوادگی: </label>
                            <input maxlength="25" class="form-control noRadius" type="text" placeholder="علیپور">
                        </div>
                    </div>
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class=""> شماره تماس: </label>
                            <input maxlength="11" class="form-control noRadius" type="text" placeholder="0912459325">
                        </div>
                    </div>
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class="">کد ملی: </label>
                            <input maxlength="11" class="form-control noRadius" type="text" placeholder="-">
                        </div>
                    </div>
                    <div class="col-md-6 uk-margin-medium-bottom">
                        <div class="">
                            <label for="" class=""> دریافت خبرنامه: </label>
                            <br>
                            <input class="uk-checkbox" type="checkbox" name="vehicle2" value="Car">
                            <span class=""> مایل به دریافت خبرنامه هستم</span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <input class="uk-button uk-button-primary  " type="submit" value="ثبت">
                <input type="reset" class="uk-button uk-button-default " value="انصراف">
            </div>
        </form>
    </div>
</div>
@endsection @section('SubMainProfile')

<div class="col-md-6">
    <p class=" uk-margin-top uk-margin-right">اطلاعات شخصی</p>
    <div class="boxProf2">
        <div class="rowCustom">
            <div class="row prof1">
                <div class="col-md-6 col-6 paddingCustom" id="nameBorder">
                    <p class="colorGray ">نام و نام خانوادگی :</p>
                    <p class="userName1 ">محمد علیپور</p>
                </div>
                <div class="col-md-6 col-6 paddingCustom" id="mailBorder">
                    <p class="colorGray ">پست الکترونیک : </p>
                    <p>MAlipor@email.com</p>
                </div>


                <div class="col-md-6 col-6 paddingCustom" id="phoneBorder">
                    <p class="colorGray ">شماره تماس : </p>
                    <p class="userName1 ">0912459325</p>
                </div>
                <div class="col-md-6 col-6 paddingCustom" id="codeBorder">
                    <p class="colorGray ">کد ملی : </p>
                    <p>-</p>
                </div>


                <div class="col-md-6 col-6 paddingCustom" id="notifBorder">
                    <p class="colorGray ">دریافت خبرنامه : </p>
                    <p class="userName1 ">بله</p>
                </div>
                <div class="col-md-6 col-6 paddingCustom">
                    <p class="colorGray ">شماره کارت : </p>
                    <p id="cardStyle">9464-****-****-***</p>
                </div>
                <div class="col-md-12 col-12 paddingCustom" id="editProf">
                    <p id="" class="colorGray  text-center editProfText" uk-toggle="target: #Change-inforamtion-Modal">ویرایش اطلاعات
                    </p>






                </div>

            </div>
        </div>
    </div>

</div>
<div class="col-md-6">
    <p class=" uk-margin-top uk-margin-right">لیست مورد علاقه ها</p>
    <div class="boxProfBookmark">
        <div class="rowCustom">
            <div class="row prof1" id="bookmarkBox">

                <div class="col-md-12 col-12 paddingCustom" id="bordeDown">
                    <img src="img/mine.jpg" alt="minerDevice" class="imgFav" id="floatRight">
                    <p id="blockText">دستگاه ماینر مدل با فناوری Z9
                    </p>
                    <div id="delPrc" class="">
                        <a href="#" uk-icon="icon: close" class="deleteBookmark" uk-toggle></a>
                    </div>
                    <p id="priceStyles">
                        <span class="text-primary">580,400</span> تومان</p>
                </div>








            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <p class=" uk-margin-top ">آخرین سفارش ها</p>
    <table class="uk-table uk-table-striped uk-table-hover" id="tableHover">
        <thead>
            <tr id="thBgcolor" class="">
                <th class="text-left">#</th>
                <th class="text-left">شماره ثبت سفارش</th>
                <th class="text-left">تاریخ ثبت سفارش</th>
                <th class="text-left">مبلغ قابل پرداخت</th>
                <th class="text-left">عملیات قابل پرداخت</th>
            </tr>
        </thead>
        <tbody>
            <tr class="">
                <td>1</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>
            <tr class="">
                <td>2</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>
            <tr class="">
                <td>3</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>
            <tr class="">
                <td>4</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="unsuccessColor">پرداخت ناموفق</td>
            </tr>
            <tr class="">
                <td>5</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="unsuccessColor">پرداخت ناموفق</td>
            </tr>
            <tr class="">
                <td>6</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>
            <tr class="">
                <td>7</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="unsuccessColor">پرداخت ناموفق</td>
            </tr>
            <tr class="">
                <td>8</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>
            <tr class="">
                <td>9</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="unsuccessColor">پرداخت ناموفق</td>
            </tr>
            <tr class="">
                <td>10</td>
                <td>MNK-2019</td>
                <td>1396/8/20</td>
                <td>34,300 تومان</td>
                <td class="successColor">پرداخت موفق</td>
            </tr>

        </tbody>
    </table>
</div>
@endsection @section('SubScriptProfile')
<script>
    $(document).ready(function () {
        $(".deleteBookmark").click(function () {
            $(this).parent().parent().hide("slide", { direction: "right" }, 500, function () {
                $(this).remove();
                if ($("#bookmarkBox").children().length == 0) {
                    $("#bookmarkBox").append("<div style='margin: 50px auto; opacity:0.9'><div style='margin: 0 auto; width : 90px;height:90px;' > <svg aria-hidden='true' data-prefix='fal' data-icon='bookmark' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 384 512' class='svg-inline--fa fa-bookmark fa-w-12 fa-5x'><path fill='rgb(153,153,153)' d='M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm16 456.287l-160-93.333-160 93.333V48c0-8.822 7.178-16 16-16h288c8.822 0 16 7.178 16 16v408.287z' ></path></svg> </div> <p class='text-center uk-margin-medium-top uk-text-meta'>محصولی در لیست علاقه مندی شما وجود ندارد</p></div>");
                }
            });
        });
    });
</script> @endsection