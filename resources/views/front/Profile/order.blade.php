@extends('layouts.ProfileMasterPage') 

@section('SubCssProfile')
<title>بیتکوین - سفارش</title>
<style>



    #tableHover tr:hover {

background-color: rgba(51, 204, 255, 0.163) !important;
}

#tableHover #thBgcolor {

background-color: rgba(72, 94, 202, 0.653) !important;
}

#tableHover th {

color: white;
}

#tableHover .successColor {
color: rgb(33, 171, 33) !important;
}

#tableHover .unsuccessColor {
color: rgba(255, 0, 0, 0.796) !important;
}


</style>
@endsection

@section('SubModalProfile')

@endsection




@section('SubMainProfile')
                <div class="col-md-12">
                    <p class=" uk-margin-top ">تمام سفارش ها</p>
                    <table class="uk-table uk-table-striped uk-table-hover" id="tableHover">
                        <thead>
                            <tr id="thBgcolor" class="">
                                <th class="text-left">#</th>
                                <th class="text-left">شماره ثبت سفارش</th>
                                <th class="text-left">تاریخ ثبت سفارش</th>
                                <th class="text-left">مبلغ قابل پرداخت</th>
                                <th class="text-left">عملیات قابل پرداخت</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="">
                                <td>1</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>
                            <tr class="">
                                <td>2</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>
                            <tr class="">
                                <td>3</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>
                            <tr class="">
                                <td>4</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="unsuccessColor">پرداخت ناموفق</td>
                            </tr>
                            <tr class="">
                                <td>5</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="unsuccessColor">پرداخت ناموفق</td>
                            </tr>
                            <tr class="">
                                <td>6</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>
                            <tr class="">
                                <td>7</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="unsuccessColor">پرداخت ناموفق</td>
                            </tr>
                            <tr class="">
                                <td>8</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>
                            <tr class="">
                                <td>9</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="unsuccessColor">پرداخت ناموفق</td>
                            </tr>
                            <tr class="">
                                <td>10</td>
                                <td>MNK-2019</td>
                                <td>1396/8/20</td>
                                <td>34,300 تومان</td>
                                <td class="successColor">پرداخت موفق</td>
                            </tr>

                        </tbody>
                    </table>
                </div>   
@endsection


@section('SubScriptProfile')
<script>
    $(document).ready(function () {

    });
</script>
@endsection