
<!doctype html>
<html lang="fa" dir="rtl"
<head>
    <title>بیتکوین</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="../../../../bitcoin-ark1/bitcoin-ark1/bitcoin/img/coin_Tg0_icon.ico" type="image/x-icon">
    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
        .successBanner {
            margin: auto;
            width: 80%;
            padding: 7px;
            background-color: #2699FB;
            border-bottom-left-radius: 8em;
            border-bottom-right-radius: 8em;
            box-shadow: 0 8px 6px -6px #707070;
        }

        .checkedCenter {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100px;
            height: auto;
            padding-top: 30px;
        }

        .successBanner h3 {
            margin-top: 20px !important;
        }

        .borderOfItem {
            border-bottom: 1px solid gray;
            opacity: 0.4;
            width: 92%;
        }

        .leftText {
            text-align: left;
        }

        .fontDorosht {
            font-size: 20px;
            ;
        }

        @media (min-width: 768px) {
            #noneMargin {
                margin-right: 7.5% !important;
            }
        }

        @media (min-width: 992px) {
            #noneMargin {
                margin-right: 7.5% !important;
            }
        }

        @media (min-width: 1200px) {
            #noneMargin {
                margin-right: 7.5% !important;
            }
        }

        @media (max-width: 812px) {
            .successBanner {
                width: 100%;
                height: 350px;
                background-color: #2699FB;
                border-bottom-left-radius: 8em;
                border-bottom-right-radius: 8em;
                box-shadow: 0 8px 6px -6px #707070;
                margin: 0px !important;
                padding: 0px !important
            }
        }

        #floatLeft {
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="successBanner w3-animate-top text-center">
            <img src="img/svg/checked.svg" class="checkedCenter" alt="Success">
            <h2 class="fontRegular text-center text-white w3-animate-right">از خرید شما متشکریم</h2>
            <br>
            <h3 class="fontRegular text-center text-white w3-animate-left">پرداخت موفقیت آمیز بود</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class=" col-md-6 col-6 fontBold uk-margin-medium-top fontDorosht" id="noneMargin">مقدار کل :</div>
            <div class=" col-md-4 col-6 fontRegular uk-margin-medium-top fontDorosht" id="floatLeft">560,234 تومان</div>
            <div class="borderOfItem uk-margin-top uk-margin-large-right"></div>
            <div class="col-md-6 col-6 fontBold uk-margin-medium-top fontDorosht" id="noneMargin">نام کالا :</div>
            <div class="col-md-4 col-6 fontRegular uk-margin-medium-top fontDorosht" id="floatLeft">MXMINER AN8 </div>
            <div class="borderOfItem uk-margin-top uk-margin-large-right"></div>
            <div class="col-md-6 col-6 fontBold uk-margin-medium-top fontDorosht" id="noneMargin">کد کالا :</div>
            <div class="col-md-4 col-6 fontRegular uk-margin-medium-top fontDorosht" id="floatLeft">MXNIER S8</div>
            <div class="borderOfItem uk-margin-top uk-margin-large-right"></div>
            <div class="col-md-6 col-6 fontBold uk-margin-medium-top fontDorosht" id="noneMargin">دارنده کالا :</div>
            <div class="col-md-4 col-6 fontRegular uk-margin-medium-top fontDorosht" id="floatLeft">محمد علیپو</div>
            <div class="borderOfItem uk-margin-top uk-margin-large-right"></div>
            <div class="col-md-12 uk-margin-top fontDorosht">
                <p class="fontRegular text-center">تا
                    <span class="text-danger">15</span> ثانیه دیگر به صفحه اصلی هدایت می شوید...</p>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-lg btn-info fontRegular uk-margin-bottom text-white">بازگشت به صفحه اصلی</button>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <!-- script -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/script.js"></script>
    <script>


        var timer=15;
        $(".text-danger").text(timer);

        setInterval(function(){ 
            timer--;
            $(".text-danger").text(timer);
            if(timer==0)
            window.location.href = "/";
         }, 1000);
    </script>
</body>

</html>