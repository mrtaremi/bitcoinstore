<!-- Postman Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postman_id', 'Postman Id:') !!}
    {!! Form::number('postman_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipmenttype Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipmenttype_id', 'Shipmenttype Id:') !!}
    {!! Form::number('shipmenttype_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Transaction Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaction_id', 'Transaction Id:') !!}
    {!! Form::number('transaction_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Send Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('send_date', 'Send Date:') !!}
    {!! Form::date('send_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivered Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivered_date', 'Delivered Date:') !!}
    {!! Form::date('delivered_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Send Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('send_address', 'Send Address:') !!}
    {!! Form::text('send_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivered Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivered_address', 'Delivered Address:') !!}
    {!! Form::text('delivered_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Post Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post_code', 'Post Code:') !!}
    {!! Form::text('post_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('shipments.index') !!}" class="btn btn-default">Cancel</a>
</div>
