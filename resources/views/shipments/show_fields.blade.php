<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $shipment->id !!}</p>
</div>

<!-- Postman Id Field -->
<div class="form-group">
    {!! Form::label('postman_id', 'Postman Id:') !!}
    <p>{!! $shipment->postman_id !!}</p>
</div>

<!-- Shipmenttype Id Field -->
<div class="form-group">
    {!! Form::label('shipmenttype_id', 'Shipmenttype Id:') !!}
    <p>{!! $shipment->shipmenttype_id !!}</p>
</div>

<!-- Transaction Id Field -->
<div class="form-group">
    {!! Form::label('transaction_id', 'Transaction Id:') !!}
    <p>{!! $shipment->transaction_id !!}</p>
</div>

<!-- Send Date Field -->
<div class="form-group">
    {!! Form::label('send_date', 'Send Date:') !!}
    <p>{!! $shipment->send_date !!}</p>
</div>

<!-- Delivered Date Field -->
<div class="form-group">
    {!! Form::label('delivered_date', 'Delivered Date:') !!}
    <p>{!! $shipment->delivered_date !!}</p>
</div>

<!-- Send Address Field -->
<div class="form-group">
    {!! Form::label('send_address', 'Send Address:') !!}
    <p>{!! $shipment->send_address !!}</p>
</div>

<!-- Delivered Address Field -->
<div class="form-group">
    {!! Form::label('delivered_address', 'Delivered Address:') !!}
    <p>{!! $shipment->delivered_address !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $shipment->description !!}</p>
</div>

<!-- Post Code Field -->
<div class="form-group">
    {!! Form::label('post_code', 'Post Code:') !!}
    <p>{!! $shipment->post_code !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $shipment->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $shipment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $shipment->updated_at !!}</p>
</div>

