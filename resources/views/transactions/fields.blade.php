<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Paymenttype Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paymenttype_id', 'Paymenttype Id:') !!}
    {!! Form::number('paymenttype_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Prices Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_prices', 'Total Prices:') !!}
    {!! Form::number('total_prices', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province_id', 'Province Id:') !!}
    {!! Form::text('province_id', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', 'City Id:') !!}
    {!! Form::text('city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Neighbourhood Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Neighbourhood_id', 'Neighbourhood Id:') !!}
    {!! Form::text('Neighbourhood_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
