<!-- Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transaction->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $transaction->user_id !!}</p>
</div>

<!-- Paymenttype Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('paymenttype_id', 'Paymenttype Id:') !!}
    <p>{!! $transaction->paymenttype_id !!}</p>
</div>

<!-- Total Prices Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('total_prices', 'Total Prices:') !!}
    <p>{!! $transaction->total_prices !!}</p>
</div>

<!-- Status Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $transaction->status !!}</p>
</div>

<!-- Province Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('province_id', 'Province Id:') !!}
    <p>{!! $transaction->province_id !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $transaction->city_id !!}</p>
</div>

<!-- Neighbourhood Id Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('Neighbourhood_id', 'Neighbourhood Id:') !!}
    <p>{!! $transaction->Neighbourhood_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $transaction->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-3 col-sm-4">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $transaction->updated_at !!}</p>
</div>

