<!doctype html>
<html lang="fa" dir="rtl">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="/img/coin_Tg0_icon.ico" type="image/x-icon">
    {{--
    <link rel="stylesheet" href="css/fonts/IRANSansWeb.ttf"> --}}

    <!-- en font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!-- Bootstrap rtl CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css" integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
        crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/css/uikit.min.css" />
    <!-- style -->
    <link rel="stylesheet" href="/css/style.css">
    @yield('SubCss')
    <style>
        
    </style>
</head>
{{-- <div id="load">


        <img src="/img/load.gif" width="60" height="60" alt=""/>
    
    
    </div> --}}
<body>
    

    
    @yield('SubModal')
    <div class="uk-offcanvas-content">
        <header class="container-fluid  fixed-top bgHeaderFirst">
            <nav class="row nav">
                <div class="col-md-1" id="logo" uk-toggle="target: #headerMobile"><a href="#">بیتکوین</a></div>

                <div class="col-md-auto" id="navx">
                    <ul>
                        <li><a href="/blog">وبلاگ</a></li>
                        <li><a href="/calculations">محاسبه درآمد</a></li>
                        <li>درباره ما</li>
                    </ul>
                </div>
                <div class="col-md-auto" id="tool">
                    <ul>
                        <!-- <li id="sech">
                                </li> -->

                        <input type="text" id="search">
                        <div id="searchIcon">
                            <span uk-icon="icon: search" class="iconToolNav"></span>
                        </div>

                        <li id="liUser">
                            <div id="drop">
                                <img src="/img/svg/user.svg" alt="" uk-svg width="20" height="20" id="shopIconMargin">
                                {{-- user name --}}

                                @if (Auth::check())

                                <span class="uk-text-small hello">سلام, محمد</span>

                                @endif
                                {{-- user name --}}

                            </div>

                            @guest

                            <div class='uk-background-muted dropHover1' uk-drop>
                                <div class='buttonAction2'>
                                    <div class='uk-button-group uk-width-1-1'>
                                        
                                        <a class='uk-button uk-width-1-1' href='/login'><span><img
                                                    src='/img/svg/sign-out.svg' uk-svg width='20' height='20' id='shopIconMargin'></span>
                                            <p> ثبت نام و ورود</p>
                                        </a></div>
                                </div>
                            </div>

                            @endguest

                            {{-- logged in --}}

                            @if (Auth::check())

                            <div class="uk-background-muted dropHover1" uk-drop>
                                <div class="colorStatus1"></div>
                                <div class="imgProf1">
                                    <div class="edit1">
                                        <img src="/img/avatar.jpg" alt="imgProf">
                                    </div>
                                    <h5 class="h5name uk-margin-small-top text-center">محمدرسول طارمی</h5>
                                </div>
                                <div class="buttonAction1">
                                    <div class="uk-button-group uk-width-1-1">
                                        <a class="uk-button uk-width-1-1" id="borderLeft1" href="#modal-overflow"
                                            uk-toggle>

                                            <span>
                                                <img src="/img/svg/user-circle.svg" alt="" uk-svg width="20" height="20"
                                                    id="shopIconMargin">
                                            </span>
                                            <p><a href="/profile">پروفایل</a></p>
                                        </a>
                                        <a class="uk-button uk-width-1-1" href="#modal-sections" uk-toggle>

                                            <span>
                                                <img src="/img/svg/sign-out.svg" alt="" uk-svg width="20" height="20" id="shopIconMargin">
                                            </span>
                                            <p>خروج از حساب</p>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            @endif

                            {{-- logged in --}}
                        </li>

                        <li>
                            <a href="/cart">
                                <span class="uk-badge bg-white text-dark badgeCustom">0</span>
                                <img src="/img/svg/shop.svg" alt="" uk-svg width="50" height="50" id="shopIconMargin">
                            </a>
                        </li>
                    </ul>
                </div>

            </nav>

            <div id="headerMobile" uk-offcanvas="mode: push;flip: true;overlay: true">
                <div class="uk-offcanvas-bar uk-flex uk-flex-column">
                    <button class="uk-offcanvas-close" type="button" uk-close></button>

                    <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-top">
                        <li>

                            <div class="uk-width-1-2@s uk-width-2-5@m">
                                <ul class="uk-nav-default " uk-nav>

                                    <!--logged-in -->

                                    <!-- <li class="uk-parent" id="!login">
                                        <a href="#" class="boxtriangle">
                                            <p class="uk-margin-top">خوش آمدی <span class="text-primary uk-text-medium uk-margin-top"
                                                    style="cursor:pointer">محمدرسول</span><span class="triangle"
                                                    data-click-state="1" uk-icon="icon: triangle-down"></span></p>

                                        </a>
                                        <ul class="uk-nav-sub">
                                            <div class="imgProf1">
                                                <div class="edit1">
                                                    <img src="/img/avatar.jpg" alt="imgProf">
                                                </div>
                                                <h5 class="h5name uk-margin-small-top text-center">محمدرسول طارمی</h5>
                                            </div>
                                            <li class=""><a class="noBorderRadius btn btn-primary uk-margin-small-bottom"
                                                    href="#">پروفایل</a></li>
                                            <li class=""><a class="noBorderRadius btn btn-danger" href="#">خروج</a></li>

                                        </ul> -->
                                        <!--logged-in -->

                                        

                                        <a href="#" class="boxtriangle">
                                                    <p class="uk-margin-small-bottom uk-margin-medium-top uk-text-medium btn btn-outline-info">ورود/عضویت</p>
                                                     
                                                </a>

                                        


                                    </li>
                                </ul>
                            </div>


                        </li>
                        <li>
                            <div class="uk-margin-remove" style="border:0.9px solid gray;"></div>
                        <li class="uk-active">
                            <a href="#">صفحه اصلی</a>
                        </li>
                        <li class="">

                            <ul class="uk-nav-sub">

                                <a href="#">وبلاگ</a>
                        </li>
                        <li>
                            <a href="#">محاسبه درآمد</a>
                        </li>
                        <li>
                            <a href="#">در باره ما</a>
                        </li>


                        <li>
                            <input style="font-size: 15px; color :#fff;" class="uk-margin-top uk-input text-center"
                                placeholder="جستجوی محصول" type="text">
                        </li>
                    </ul>
                    </li>
                    </ul>

                </div>
            </div>
        </header>



        <main uk-scrollspy="cls: uk-animation-fade; target: > div; delay: 20;">


            @yield('SubMain')

        </main>



        <!-- start footer -->
        <footer class="container-fluid">
            <br>
            <div class="row fontRegular">
                <div class="col-md-3 col-sm-12 col-lg-3 col-6 col-xl-3">
                    <p>تست تست تست</p>
                    <p>تست تست</p>
                    <p>تست</p>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3 col-6 col-xl-3">
                    <p>تست تست تست</p>
                    <p>تست تست</p>
                    <p>تست</p>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3 col-6 col-xl-3">
                    <p>تست تست تست</p>
                    <p>تست تست</p>
                    <p>تست</p>
                </div>
                <div class="col-md-3 col-sm-12 col-lg-3 col-6 col-xl-3">
                    {{--
                    <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" alt=""
                        srcset="">--}} {{--
                    <img src="http://www.stickpng.com/assets/images/5847e95fcef1014c0b5e4822.png" alt="" srcset="">--}}
                </div>
            </div>
        </footer>
        <!-- end footer -->
    </div>


    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.0.0/js/bootstrap.min.js" integrity="sha384-54+cucJ4QbVb99v8dcttx/0JRx4FHMmhOWi4W+xrXpKcsKQodCBwAvu3xxkZAwsH"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.17/js/uikit-icons.min.js"></script>

    <!-- script -->

    
    <script src="/js/script.js"></script>
    @yield('SubScript')
</body>

</html>