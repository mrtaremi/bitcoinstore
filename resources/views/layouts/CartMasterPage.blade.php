@extends('layouts.MasterPage')
 @section('SubCss')
<link rel="stylesheet" href="/css/MasterCart.css">
@yield('SubCssCart')
@endsection
@section('SubModal')
@yield('SubModalCart')
@endsection
@section('SubMain')
<div class="container-fluid allDetails">

    <div class="row">
        
        @yield('SubMainCart')

        <div class="col-md-3 ">
            <div class="cardDetails">
                <div class="row">
                    <div class="col-md-6 col-6 ">
                       <div>
                       <p class="right-align">مبلغ کل (<span id="count"></span>)</p>
                       </div>
                    </div>
                    <div class="col-md-6 col-6 ">
                        <div>
                        <p class="left-align">تومن <span id="total"></span></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-6 ">
                        <div>
                        <p class="fontRegular right-align">سود شما از خرید</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-6 ">
                        <div>
                        <p class="left-align">
                            <span class="text-primary" id="off"></span> تومان</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-6 ">
                        <div>
                        <p class="right-align">هزینه ارسال
                        </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-6 ">
                        <div>
                        <p class="left-align"> وابسته به آدرس
                        </p>
                        </div>
                    </div>
                </div>
                <hr>
                <hr>
                <div class="col-md-12 col-12" id="btnContinue">
                    <p class="fontRegular text-center text-danger"> مبلغ قابل پرداخت </p>
                    <p class="fontRegular text-center ">تومن <span id="cash"></span></p>
                    <button class="btn btn-primary btn-block">
                        <span uk-icon="icon: arrow-right ; ratio : 1.5" id="iconFlash"></span>
                        <a href="" class="fontRegular text-center"> ادامه ثبت سفارش</a>
                    </button>
                    <p class="fontRegular uk-text-small text-center uk-text-meta">کالا های موجود شما ثبت و رزرو شده اند ، برای
                        نهایی کردن سفارش مراحل بعدی را تکمیل کنید</p>
                </div>
            </div>
            <div class="supportDetails1 uk-margin-top">
                <div class="row">
                    <div class="col-md-12">
                        <div class="iconAndText bg-white">
                            <p class="fontRegular">
                                <span uk-icon="future"></span>هفت روز ضمانت تعویض</p>
                            <p class="fontRegular">
                                <span uk-icon="credit-card"></span>پرداخت در محل با کارت بانکی </p>
                            <p class="fontRegular">
                                <span uk-icon="bolt"></span>تحویل اکسپرس</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection
@section('SubScript')
<script>
    $(document).ready(function(){
        
    });
</script>
@yield('SubScriptCart')
@endsection