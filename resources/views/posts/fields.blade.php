<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6" id="boxInputsFirst">

    <div class="row" id="boxInputsSecend">

        <div class="form-group col-sm-12 boxInput">

            {!! Form::label('main_file', 'Main Image :') !!}

            <input type="file" name="main_file" class="form-control">

        </div>
    </div>

</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <br>

    {!! Form::textarea('description', null, ['class' => 'form-control','maxlength'=>'280']) !!}

</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">

    {!! Form::label('summernoteInput', 'Content:') !!}


</br>

    <textarea name="summernoteInput" class="summernote"></textarea>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('posts.index') !!}" class="btn btn-default">Cancel</a>
</div>
