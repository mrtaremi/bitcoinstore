@extends('layouts.MasterPage')
@section('SubCss')
<title>بیتکوین - صفحه اصلی</title>
@endsection
@section('SubMain')
<br>
        <div class="col-md-12" id="slider1">
            <div class="uk-position-relative uk-visible-toggle uk-light " uk-slideshow="animation: scale;min-height: 300; max-height: 300"
                 id="sliderBox">

                <ul class="uk-slideshow-items">

                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-bottom-left">
                            <img src="https://steemitimages.com/p/x7L2VSNEiyA8Ez5tNApvCaiPSdodqgF9XbUT22gv4BxMnQ5Re6NQKhef3PiR2RXwxQso36yJMuRovQE?format=match&mode=fit"
                                 alt="" uk-cover>
                        </div>
                    </li>
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-bottom-left">
                            <img src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F1036378723%2F960x0.jpg%3Ffit%3Dscale"
                                 alt="" uk-cover>
                        </div>
                    </li>
                    <li>
                        <div class="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-bottom-left">
                            <img src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F1036378723%2F960x0.jpg%3Ffit%3Dscale"
                                 alt="" uk-cover>
                        </div>
                    </li>

                </ul>

                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                <!-- <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin uk-position-bottom-center" id=""></ul> -->
                <div class="uk-position-bottom-center uk-position-small" id="sliderButton">
                    <ul class="uk-dotnav">
                        <li uk-slideshow-item="0">
                            <a href="#">Item 1</a>
                        </li>
                        <li uk-slideshow-item="1">
                            <a href="#">Item 2</a>
                        </li>
                        <li uk-slideshow-item="2">
                            <a href="#">Item 3</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <br>
        <div class="col-md-12" id="slider2">
            <div class="uk-position-relative uk-visible-toggle uk-light" uk-slider="center: true">

                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m uk-grid uk-grid-match" uk-height-viewport="offset-top: true; offset-bottom: 30">
                    <li class="uk-width-3-4">
                        <div class="uk-cover-container">
                            <img src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F1036378723%2F960x0.jpg%3Ffit%3Dscale" alt="" uk-cover>

                        </div>
                    </li>
                    <li class="uk-width-3-4">
                        <div class="uk-cover-container">
                            <img src="https://i.pinimg.com/564x/76/72/4c/76724c1a92f2448f91328d6881fc831d.jpg" alt="" uk-cover>

                        </div>
                    </li>
                    <li class="uk-width-3-4">
                        <div class="uk-cover-container">
                            <img src="https://i.pinimg.com/564x/76/72/4c/76724c1a92f2448f91328d6881fc831d.jpg" alt="" uk-cover>

                        </div>
                    </li>
                    <li class="uk-width-3-4">
                        <div class="uk-cover-container">
                            <img src="https://i.pinimg.com/564x/76/72/4c/76724c1a92f2448f91328d6881fc831d.jpg" alt="" uk-cover>

                        </div>
                    </li>
                    <li class="uk-width-3-4">
                        <div class="uk-cover-container">
                            <img src="https://i.pinimg.com/564x/76/72/4c/76724c1a92f2448f91328d6881fc831d.jpg" alt="" uk-cover>

                        </div>
                    </li>
                </ul>

                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>
        </div>
        <br>
        <div class="col-md-12" id="slider3">
            <div class="uk-position-relative uk-visible-toggle uk-light" uk-slider>

                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m uk-grid">
                    
                    <li class="uk-transition-toggle">
                        <div class="uk-panel">
                            <img src="https://media.istockphoto.com/vectors/bitcoin-golden-coin-the-symbol-of-the-crypto-currency-on-horizontal-vector-id918372214 "
                                 alt="">
                            <div class="uk-position-center uk-panel uk-transition-slide-bottom">
                                <h1>3</h1>
                            </div>
                        </div>
                    </li>
                    <li class="uk-transition-toggle">
                        <div class="uk-panel">
                            <img src="https://media.istockphoto.com/vectors/bitcoin-golden-coin-the-symbol-of-the-crypto-currency-on-horizontal-vector-id918372214"
                                 alt="">
                            <div class="uk-position-center uk-panel uk-transition-slide-bottom">
                                <h1>4</h1>
                            </div>
                        </div>
                    </li>
                    <li class="uk-transition-toggle">
                        <div class="uk-panel">
                            <img src="https://media.istockphoto.com/vectors/bitcoin-golden-coin-the-symbol-of-the-crypto-currency-on-horizontal-vector-id918372214"
                                 alt="">
                            <div class="uk-position-center uk-panel uk-transition-slide-bottom">
                                <h1>5</h1>
                            </div>
                        </div>
                    </li>

                </ul>

                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>
        </div>
        <br>
        <br>
        <br>

        <div class="container-fluid uk-background-muted" id="products">
            <div class="container">
                <br>
                <h1 class="uk-heading-line uk-text-center">
                    <span>محصولات</span>
                </h1>
                <br>
                <br>
                <div class="row" id="addproduct">



                    {{--<div class="col-md-4 col-sm-6 col-lg-6 col-xl-4 product uk-margin-large-bottom">--}}
                        {{--<div class="boxProducts uk-background-muted uk-margin uk-transition-toggle" tabindex="0">--}}
                            {{--<a href="http://google.com"><img class="uk-align-center uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/light.jpg"--}}
                                {{--alt="" srcset=""></a>--}}
                            {{--<div class="textBox">--}}
                                {{--<a href="http://google.com"><h4 class="uk-text-center">ANT MINER S8</h4></a>--}}
                                {{--<h4 class="uk-text-center uk-text-bold text-primary">موجود</h4>--}}
                                {{--<div class="heightText">--}}
                                {{--<p class="text-justify  uk-margin-medium-top" id="detailsBody">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون...</p>--}}
                                {{--</div>--}}
                                {{--<hr>--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<div class="bookmarkAction1">--}}
                                            {{--<span uk-icon="icon: bookmark" class="deleteBtn bookmarked"></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<p class="uk-align-left price1">4,345,000 تومان</p>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-sm-6 col-lg-6 col-xl-4 product uk-margin-large-bottom">--}}
                        {{--<div class="boxProducts uk-background-muted uk-margin uk-transition-toggle" tabindex="0">--}}
                            {{--<a href="http://google.com"><img class="uk-align-center uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/light.jpg"--}}
                                {{--alt="" srcset=""></a>--}}
                            {{--<div class="textBox">--}}
                                {{--<a href="http://google.com"><h4 class="uk-text-center">ANT MINER S8</h4></a>--}}
                                {{--<h4 class="uk-text-center uk-text-bold text-primary">موجود</h4>--}}
                                {{--<div class="heightText">--}}
                                {{--<p class="text-justify  uk-margin-medium-top" id="detailsBody">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون...</p>--}}
                                {{--</div>--}}
                                {{--<hr>--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<div class="bookmarkAction1">--}}
                                            {{--<span uk-icon="icon: bookmark" class="deleteBtn bookmarked"></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<p class="uk-align-left price1">4,345,000 تومان</p>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-sm-6 col-lg-6 col-xl-4 product uk-margin-large-bottom">--}}
                        {{--<div class="boxProducts uk-background-muted uk-margin uk-transition-toggle" tabindex="0">--}}
                            {{--<a href="http://google.com"><img class="uk-align-center uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/light.jpg"--}}
                                {{--alt="" srcset=""></a>--}}
                            {{--<div class="textBox">--}}
                                {{--<a href="http://google.com"><h4 class="uk-text-center">ANT MINER S8</h4></a>--}}
                                {{--<h4 class="uk-text-center uk-text-bold text-primary">موجود</h4>--}}
                                {{--<div class="heightText">--}}
                                {{--<p class="text-justify  uk-margin-medium-top" id="detailsBody">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون...</p>--}}
                                {{--</div>--}}
                                {{--<hr>--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<div class="bookmarkAction1">--}}
                                            {{--<span uk-icon="icon: bookmark" class="deleteBtn bookmarked"></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<p class="uk-align-left price1">4,345,000 تومان</p>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-sm-6 col-lg-6 col-xl-4 product uk-margin-large-bottom">--}}
                        {{--<div class="boxProducts uk-background-muted uk-margin uk-transition-toggle" tabindex="0">--}}
                            {{--<a href="http://google.com"><img class="uk-align-center uk-transition-scale-up uk-transition-opaque" src="https://getuikit.com/docs/images/light.jpg"--}}
                                {{--alt="" srcset=""></a>--}}
                            {{--<div class="textBox">--}}
                                {{--<a href="http://google.com"><h4 class="uk-text-center">ANT MINER S8</h4></a>--}}
                                {{--<h4 class="uk-text-center uk-text-bold text-primary">موجود</h4>--}}
                                {{--<div class="heightText">--}}
                                {{--<p class="text-justify  uk-margin-medium-top" id="detailsBody">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها  ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون...</p>--}}
                                {{--</div>--}}
                                {{--<hr>--}}

                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<div class="bookmarkAction1">--}}
                                            {{--<span uk-icon="icon: bookmark" class="deleteBtn bookmarked"></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-6">--}}
                                        {{--<p class="uk-align-left price1">4,345,000 تومان</p>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}






                </div>
                {{-- <h1 class="uk-heading-line uk-text-center uk-margin-remove-bottom" id="btnLoadProduct"><span class="uk-text-small">محصولات بیشتر <br><span uk-icon="icon: chevron-down; ratio: 2"></span></span></h1> --}}
            </div>
        </div>
@endsection
@section('SubScript')
<script async="async">

    $(function(){

        $.get("/buyershowall",function (data,status) {

            $.each(data.products, function (i,e) {

                var stock;

                switch (e.status) {
                    case 1:
                        stock='موجود در انبار';
                        break;
                    case 2:
                        stock='موجود';
                        break;
                    case 3:
                        stock='ناموجود';
                        break;
                    case 4:
                        stock='به زودی';
                        break;
                }
                $("#addproduct").append("<div class='col-md-4 col-sm-6 col-lg-6 col-xl-4 product uk-margin-large-bottom'><div class='boxProducts uk-background-muted uk-margin uk-transition-toggle' tabindex='0'><a target='_blank' href='/product/"+e.id+"'><img class='uk-align-center uk-transition-scale-up uk-transition-opaque' src='/"+e.avatar_image_path+"'></a><div class='textBox'><a href='/product/"+e.id+"><h4 class='uk-text-center'>"+e.name+"</h4></a><h4 class='uk-text-center uk-text-bold text-primary'>"+stock+"</h4><div class='heightText'><p class='text-justify  uk-margin-medium-top' id='detailsBody'>"+e.main_description+"</p></div><hr><div class='row'><div class='col-md-6 col-6'><div class='bookmarkAction1'><span uk-icon='icon: bookmark' class='deleteBtn bookmarked'></span></div></div><div class='col-md-6 col-6'><p class='uk-align-left price1'>"+colon(e.price_per_unit.toString())+"</p></div></div></div></div></div>");

            // $(document).ready(function () {
            //
            //     $(".bookmarked svg polygon").addClass("defaultBokmarked");
            //     $(".bookmarked svg").addClass("clickForSize");
            //
            //     $(".bookmarked").click(function () {
            //         $(this).find("svg polygon").toggleClass("clickedBookmarked", 1000);
            //         $(this).find("svg").toggleClass("clickedForSize", 1000);
            //
            //     });
            //
            //
            //
            // });

        });

            $(document).ready(function () {

                $(".bookmarked svg polygon").addClass("defaultBokmarked");
                $(".bookmarked svg").addClass("clickForSize");

                $(".bookmarked").click(function () {
                    $(this).find("svg polygon").toggleClass("clickedBookmarked", 1000);
                    $(this).find("svg").toggleClass("clickedForSize", 1000);

                });

            });

    });


    });


</script>
@endsection