@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Allsell
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($allsell, ['route' => ['allsells.update', $allsell->id], 'method' => 'patch']) !!}

                        @include('allsells.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection