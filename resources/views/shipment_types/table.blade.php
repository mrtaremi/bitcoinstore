<table class="table table-responsive" id="shipmentTypes-table">
    <thead>
        <tr>
            <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($shipmentTypes as $shipmentType)
        <tr>
            <td>{!! $shipmentType->name !!}</td>
            <td>
                {!! Form::open(['route' => ['shipmentTypes.destroy', $shipmentType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('shipmentTypes.show', [$shipmentType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('shipmentTypes.edit', [$shipmentType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>