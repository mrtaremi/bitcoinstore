<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

<!-- Product Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_type_id', 'Product Type Id:') !!}
    {!! Form::number('product_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Market Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('market_id', 'Market Id:') !!}
    {!! Form::number('market_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Special Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_service_id', 'Special Service Id:') !!}
    {!! Form::number('special_service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rank', 'Rank:') !!}
    {!! Form::number('rank', null, ['class' => 'form-control']) !!}
</div>

<!-- Weight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('weight', 'Weight:') !!}
    {!! Form::number('weight', null, ['class' => 'form-control']) !!}
</div>

<!-- Off Field -->
<div class="form-group col-sm-6">
    {!! Form::label('off', 'Off:') !!}
    {!! Form::number('off', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Dimensions Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dimensions', 'Dimensions:') !!}
    {!! Form::text('dimensions', null, ['class' => 'form-control']) !!}
</div>

<!-- Colors Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colors', 'Colors:') !!}
    {!! Form::text('colors', null, ['class' => 'form-control']) !!}
</div>

<!-- Gas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gas', 'Gas:') !!}
    {!! Form::text('gas', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">

    <input type="hidden" name="status" id="statusinput">

    {!! Form::label('status', 'Status:') !!}
    <select id="status" class="form-control">
        <option value="1">In Store Room</option>
        <option value="2">In Stock But Not In Storeroom</option>
        <option value="3">Not Stock</option>
        <option value="4">Comming Soon</option>
    </select>
</div>

<!-- Poweruse Field -->
<div class="form-group col-sm-6">
    {!! Form::label('poweruse', 'Poweruse:') !!}
    {!! Form::number('poweruse', null, ['class' => 'form-control' ,  'step'=>'0.00000001']) !!}
</div>

<!-- Btc Per Hour Field -->
<div class="form-group col-sm-6">
    {!! Form::label('btc_per_hour', 'Btc Per Hour:') !!}
    {!! Form::number('btc_per_hour', null, ['class' => 'form-control', 'step'=>'0.00000001']) !!}
</div>

<!-- Price Per Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_per_unit', 'Price Per Unit:') !!}
    {!! Form::number('price_per_unit', null, ['class' => 'form-control']) !!}
</div>

<!-- Main Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_description', 'Main Description:') !!}
    {!! Form::textarea('main_description', null, ['class' => 'form-control','maxlength'=>'500']) !!}
</div>

<!-- Sub Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_description', 'Sub Description:') !!}
    {!! Form::textarea('sub_description', null, ['class' => 'form-control','maxlength'=>'1500']) !!}
</div>
<!-- add and delete -->
<div class="form-group col-sm-6">
    {!! Form::label('main_file', 'Main Image :') !!}
    <div class="row">

        <div class="col-sm-3">
            <div class="btn btn-sm btn-primary btn-rounded" id="add"><i class="fas fa-plus mr-1"></i> Add</div>
        </div>
        <div class="col-sm-3">
            <div class="btn btn-sm btn-danger btn-rounded" id="remove"><i class="fas fa-minus mr-1"></i> remove</div>
        </div>
    </div>
</div>

<div class="form-group col-sm-6" id="boxInputsFirst">
    <div class="row" id="boxInputsSecend">

        <!-- Sub Description Field -->
        <div class="form-group col-sm-12 boxInput">

            <input type="hidden" name="totalNumberOfInput" id="totalNumberOfInput">

            {!! Form::label('main_file', 'Main Image :') !!}

            <input type="file" name="main_file" class="form-control">
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary','id'=>'Save']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>


<script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>

<script>

    $(function () {
       $('#Save').click(function (e) {
           document.getElementById('statusinput').value=document.getElementById('status').value;
       });
    });

    $(function(){

        var j=0;

        $("#add").click(function(e) {

            // alert('add');

            j++;

            $("#boxInputsSecend").append('<div class="form-group col-sm-12 boxInput"><label> Sub Image : '+j+' </lable><input type="file" name="sub_file'+j+'" class="form-control"></div>');

            $("#totalNumberOfInput").val(j);

        });


        $("#remove").click(function(e) {

            // alert("remove");

            if($("#boxInputsSecend .boxInput").length > 1){

                j--;

                $("#boxInputsSecend .boxInput:last-child").remove();

                $("#totalNumberOfInput").val(j);
            }else{
                toastr.error('The minimum input is one');
            }
        });



    });



</script>



