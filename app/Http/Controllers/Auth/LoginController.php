<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $role_id=auth()->user()->role_id ;
        Auth::logout();

        if ($role_id ==1) {
            return redirect('/admin-panel/login');
        }

        return redirect('/');
    }

    public function redirectTo(){

        // User role
        $role = Auth::user()->role_id ;

        // Check user role
        switch ($role) {
            case 1:
                return '/admin-panel';
                break;
            default:
                return '/';
                break;
        }
    }

}
