<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Post;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $posts = $this->postRepository->all(['id','title']);

        return view('posts.index')
            ->with('posts', $posts);
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $detail=$request->summernoteInput;

//        dd($detail);

        $dom = new \domdocument();

        $dom->loadHtml(mb_convert_encoding($detail, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){
            $data = $img->getattribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $image_name= time().$k.'.png';
            $path = public_path().'/File/Post/'.$request->id.'/images' .'/'. $image_name;

            file_put_contents($path, $data);

            $img->removeattribute('src');
            $img->setattribute('src','/File/Post/'.$request->id.'/images' .'/'. $image_name);
        }

        $detail = $dom->savehtml();

        $post_id=$this->postRepository->create([

            'content'=>$detail,
            'title'=>$request->title,
            'user_id'=>auth()->user()->id,
            'description'=>$request->description,

        ])['id'];



        $main_file = 'main'.'.'.$request->file('main_file')->getClientOriginalExtension();

        $this->postRepository->update([

            'avatar_image_path'=>'File/Post/'.$post_id.'/images/'.$main_file,

        ],$post_id);

        $request->file('main_file')->move(public_path('File/Post/'.$post_id.'/images'),$main_file);

        return redirect('/blog/post/'.$post_id);

    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return redirect('/blog/post/'.$id);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $detail=$request->summernoteInput;

        $dom = new \domdocument();

        $dom->loadHtml(mb_convert_encoding($detail, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){
            $data = $img->getattribute('src');

            try{

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);

                $data = base64_decode($data);
                $image_name= time().$k.'.png';
                $path = public_path().'/File/Post/'.$id.'/images' .'/'. $image_name;

                file_put_contents($path, $data);

                $img->removeattribute('src');
                $img->setattribute('src','/File/Post/'.$id.'/images' .'/'. $image_name);

            }
            catch (\Exception $e){}


        }

        $detail = $dom->savehtml();


        if ($request->file('main_file')!=null){

            $main_file = 'main'.'.'.$request->file('main_file')->getClientOriginalExtension();

            $this->postRepository->update([

                'avatar_image_path'=>'File/Post/'.$id.'/images/'.$main_file,
                'content'=>$detail,
                'title'=>$request->title,
                'description'=>$request->description,

            ],$id);

            $request->file('main_file')->move(public_path('File/Post/'.$id.'/images'),$main_file);

        }
        else{

            $this->postRepository->update([
                
                'content'=>$detail,
                'title'=>$request->title,
                'description'=>$request->description,

            ],$id);

        }

        return redirect('/blog/post/'.$id);

    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        $this->postRepository->delete($id);

        Flash::success('Post deleted successfully.');

        return redirect(route('posts.index'));
    }
}
