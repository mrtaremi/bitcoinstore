<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{

    private $productRepository;


    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    public function index(Request $request){



        if ($this->productRepository->findWithoutFail($request->id,['id'])==null){

            return response()->view('404', [], 404);

        }

        return view('front.product')
            ->with('id',$request->id);
    }
}
