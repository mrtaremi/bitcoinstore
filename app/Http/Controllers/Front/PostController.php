<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    public function show($id){
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {

            return view('404');
        }

        return view('front.post')->with('post', $post);
    }

    public function index(){

        return view('front.post');
    }
}
