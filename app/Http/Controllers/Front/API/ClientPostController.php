<?php

namespace App\Http\Controllers\Front\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ClientPostController extends Controller
{
    private $postRepository;
    private $userRepository;

    public function __construct(PostRepository $postRepo,UserRepository $userRepository)
    {
        $this->postRepository = $postRepo;
        $this->userRepository = $userRepository;
    }

    public function show($id)
    {
        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return response()->json([
                'status'=>'404'
            ]);
        }

        return response()->json($post);

    }

    public function showall()
    {
        $posts = $this->postRepository->orderBy('created_at','dsc')->all(
            [
                'id',
                'title',
                'avatar_image_path',
                'created_at',
                'updated_at',
                'description',
                'user_id',
            ]
        );

        foreach ($posts as $post){

            $post['user_name']=$this->userRepository->find((int)$post['user_id'])['first_name'];

            unset($post['user_id']);

        }

        return response()->json(['posts'=>$posts]);

    }

}
