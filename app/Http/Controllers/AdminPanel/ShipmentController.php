<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Requests\CreateShipmentRequest;
use App\Http\Requests\UpdateShipmentRequest;
use App\Repositories\ShipmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;

class ShipmentController extends AppBaseController
{
    /** @var  ShipmentRepository */
    private $shipmentRepository;

    public function __construct(ShipmentRepository $shipmentRepo)
    {
        $this->shipmentRepository = $shipmentRepo;
    }

    /**
     * Display a listing of the Shipment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $this->shipmentRepository->pushCriteria(new RequestCriteria($request));
        $shipments = $this->shipmentRepository->all();

        return view('shipments.index')
            ->with('shipments', $shipments);
    }

    /**
     * Show the form for creating a new Shipment.
     *
     * @return Response
     */
    public function create()
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('shipments.create');
    }

    /**
     * Store a newly created Shipment in storage.
     *
     * @param CreateShipmentRequest $request
     *
     * @return Response
     */
    public function store(CreateShipmentRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $input = $request->all();

        $shipment = $this->shipmentRepository->create($input);

        Flash::success('Shipment saved successfully.');

        return redirect(route('shipments.index'));
    }

    /**
     * Display the specified Shipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        return view('shipments.show')->with('shipment', $shipment);
    }

    /**
     * Show the form for editing the specified Shipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        return view('shipments.edit')->with('shipment', $shipment);
    }

    /**
     * Update the specified Shipment in storage.
     *
     * @param  int              $id
     * @param UpdateShipmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShipmentRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        $shipment = $this->shipmentRepository->update($request->all(), $id);

        Flash::success('Shipment updated successfully.');

        return redirect(route('shipments.index'));
    }

    /**
     * Remove the specified Shipment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $shipment = $this->shipmentRepository->findWithoutFail($id);

        if (empty($shipment)) {
            Flash::error('Shipment not found');

            return redirect(route('shipments.index'));
        }

        $this->shipmentRepository->delete($id);

        Flash::success('Shipment deleted successfully.');

        return redirect(route('shipments.index'));
    }
}
