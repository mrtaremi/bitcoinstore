<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;

class AdminPanelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('adminpanel');

    }
}
