<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Requests\CreatePostmanRequest;
use App\Http\Requests\UpdatePostmanRequest;
use App\Repositories\PostmanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;

class PostmanController extends AppBaseController
{
    /** @var  PostmanRepository */
    private $postmanRepository;

    public function __construct(PostmanRepository $postmanRepo)
    {
        $this->postmanRepository = $postmanRepo;
    }

    /**
     * Display a listing of the Postman.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $this->postmanRepository->pushCriteria(new RequestCriteria($request));
        $postmen = $this->postmanRepository->all();

        return view('postmen.index')
            ->with('postmen', $postmen);
    }

    /**
     * Show the form for creating a new Postman.
     *
     * @return Response
     */
    public function create()
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('postmen.create');
    }

    /**
     * Store a newly created Postman in storage.
     *
     * @param CreatePostmanRequest $request
     *
     * @return Response
     */
    public function store(CreatePostmanRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $input = $request->all();

        $postman = $this->postmanRepository->create($input);

        Flash::success('Postman saved successfully.');

        return redirect(route('postmen.index'));
    }

    /**
     * Display the specified Postman.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $postman = $this->postmanRepository->findWithoutFail($id);

        if (empty($postman)) {
            Flash::error('Postman not found');

            return redirect(route('postmen.index'));
        }

        return view('postmen.show')->with('postman', $postman);
    }

    /**
     * Show the form for editing the specified Postman.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $postman = $this->postmanRepository->findWithoutFail($id);

        if (empty($postman)) {
            Flash::error('Postman not found');

            return redirect(route('postmen.index'));
        }

        return view('postmen.edit')->with('postman', $postman);
    }

    /**
     * Update the specified Postman in storage.
     *
     * @param  int              $id
     * @param UpdatePostmanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostmanRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $postman = $this->postmanRepository->findWithoutFail($id);

        if (empty($postman)) {
            Flash::error('Postman not found');

            return redirect(route('postmen.index'));
        }

        $postman = $this->postmanRepository->update($request->all(), $id);

        Flash::success('Postman updated successfully.');

        return redirect(route('postmen.index'));
    }

    /**
     * Remove the specified Postman from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $postman = $this->postmanRepository->findWithoutFail($id);

        if (empty($postman)) {
            Flash::error('Postman not found');

            return redirect(route('postmen.index'));
        }

        $this->postmanRepository->delete($id);

        Flash::success('Postman deleted successfully.');

        return redirect(route('postmen.index'));
    }
}
