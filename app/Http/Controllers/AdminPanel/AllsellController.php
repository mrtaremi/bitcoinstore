<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Requests\CreateAllsellRequest;
use App\Http\Requests\UpdateAllsellRequest;
use App\Repositories\AllsellRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;

class AllsellController extends AppBaseController
{

    private $allsellRepository;

    public function __construct(AllsellRepository $allsellRepo)
    {
        $this->allsellRepository = $allsellRepo;
    }

    /**
     * Display a listing of the Allsell.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

            $this->allsellRepository->pushCriteria(new RequestCriteria($request));
            $allsells = $this->allsellRepository->all();

            return view('allsells.index')
                ->with('allsells', $allsells);

    }

    /**
     * Show the form for creating a new Allsell.
     *
     * @return Response
     */
    public function create()
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('allsells.create');
    }

    /**
     * Store a newly created Allsell in storage.
     *
     * @param CreateAllsellRequest $request
     *
     * @return Response
     */
    public function store(CreateAllsellRequest $request)
    {

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

            $input = $request->all();

            $allsell = $this->allsellRepository->create($input);

            Flash::success('Allsell saved successfully.');

            return redirect(route('allsells.index'));
    }

    /**
     * Display the specified Allsell.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }
            $allsell = $this->allsellRepository->findWithoutFail($id);

            if (empty($allsell)) {
                Flash::error('Allsell not found');

                return redirect(route('allsells.index'));
            }

            return view('allsells.show')->with('allsell', $allsell);

    }

    /**
     * Show the form for editing the specified Allsell.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

            $allsell = $this->allsellRepository->findWithoutFail($id);

            if (empty($allsell)) {
                Flash::error('Allsell not found');

                return redirect(route('allsells.index'));
            }

            return view('allsells.edit')->with('allsell', $allsell);

    }

    /**
     * Update the specified Allsell in storage.
     *
     * @param  int              $id
     * @param UpdateAllsellRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAllsellRequest $request)
    {

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

            $allsell = $this->allsellRepository->findWithoutFail($id);

            if (empty($allsell)) {
                Flash::error('Allsell not found');

                return redirect(route('allsells.index'));
            }

            $allsell = $this->allsellRepository->update($request->all(), $id);

            Flash::success('Allsell updated successfully.');

            return redirect(route('allsells.index'));

    }

    /**
     * Remove the specified Allsell from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

            $allsell = $this->allsellRepository->findWithoutFail($id);

            if (empty($allsell)) {
                Flash::error('Allsell not found');

                return redirect(route('allsells.index'));
            }

            $this->allsellRepository->delete($id);

            Flash::success('Allsell deleted successfully.');

            return redirect(route('allsells.index'));

    }
}
