<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;

use App\Repositories\ProductRepository;
use App\Repositories\ImageRepository;
use App\Repositories\CommentRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Mockery\Exception;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Carbon\Carbon;
use App\Http\Controllers\AdminPanel\CheckRoleAdmin;


class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;
    private $imageRepository;
//    private $commentrepository;

    public function __construct(ProductRepository $productRepo,ImageRepository $imageRepository,CommentRepository $commentrepository)
    {
        $this->productRepository = $productRepo;
        $this->imageRepository=$imageRepository;
//        $this->$commentrepository=$commentrepository;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $this->productRepository->pushCriteria(new RequestCriteria($request));
        $products = $this->productRepository->all();

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        return view('products.create');
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $input = $request->all();

        $product_id=$this->productRepository->create($input)['id'];

        $main_file = 'main'.'.'.$request->file('main_file')->getClientOriginalExtension();

        $this->productRepository->update([

            'avatar_image_path'=>'File/Product/'.$product_id.'/images/'.$main_file,

        ],$product_id);

        $request->file('main_file')->move(public_path('File/Product/'.$product_id.'/images'),$main_file);

        for($i=1;$i<=$request['totalNumberOfInput'];$i++){

            $sub_file = 'sub'.$i.'.'.$request->file('sub_file'.$i)->getClientOriginalExtension();

            $request->file('sub_file'.$i)->move(public_path('File/Product/'.$product_id.'/images'),$sub_file);

            $this->imageRepository->create([

                'path'=>'File/Product/'.$product_id.'/images/'.$sub_file,
                'product_id'=>$product_id,
                'role'=>$i,

             ]);

        }

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {

        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $product = $this->productRepository->update($request->all(), $id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        if($request->file('main_file')!=null){

            $main_file = 'main'.'.'.$request->file('main_file')->getClientOriginalExtension();

            $format=explode(".",$id.$product['avatar_image_path']);

            rename(public_path($product['avatar_image_path']), public_path('File/Trash/'.Carbon::now().'-Product-MainAvatar-'.$id.'.'.$format[sizeof($format)-1]));

            $request->file('main_file')->move(public_path('File/Product/'.$id.'/images'),$main_file);

            $this->productRepository->update([
                'avatar_image_path'=>'File/Product/'.$id.'/images/'.$main_file
            ], $id);
        }



        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(!CheckRoleAdmin::CheckRoleAdmin(auth()->user()->role_id)) {

            return view('404');

        }

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
}
