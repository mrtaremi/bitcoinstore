<?php

namespace App\Repositories;

use App\Models\Postman;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PostmanRepository
 * @package App\Repositories
 * @version October 16, 2018, 11:17 am UTC
 *
 * @method Postman findWithoutFail($id, $columns = ['*'])
 * @method Postman find($id, $columns = ['*'])
 * @method Postman first($columns = ['*'])
*/
class PostmanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'const_number',
        'identity_number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Postman::class;
    }
}
