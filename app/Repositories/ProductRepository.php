<?php

namespace App\Repositories;

use App\Models\Product;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version October 23, 2018, 9:18 am UTC
 *
 * @method Product findWithoutFail($id, $columns = ['*'])
 * @method Product find($id, $columns = ['*'])
 * @method Product first($columns = ['*'])
*/
class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_type_id',
        'market_id',
        'special_service_id',
        'rank',
        'weight',
        'off',
        'name',
        'dimensions',
        'colors',
        'gas',
        'main_description',
        'sub_description',
        'status',
        'poweruse',
        'btc_per_hour',
        'avatar_image_path',
        'price_per_unit'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
