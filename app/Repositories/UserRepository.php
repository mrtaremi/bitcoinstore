<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version October 22, 2018, 3:07 pm UTC
 *
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role_id',
        'email',
        'password',
        'first_name',
        'last_name',
        'company_name',
        'phone_number',
        'const_number',
        'remember_token',
        'avatar_path'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
