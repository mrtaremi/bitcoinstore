<?php

namespace App\Repositories;

use App\Models\Allsell;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AllsellRepository
 * @package App\Repositories
 * @version October 22, 2018, 2:21 pm UTC
 *
 * @method Allsell findWithoutFail($id, $columns = ['*'])
 * @method Allsell find($id, $columns = ['*'])
 * @method Allsell first($columns = ['*'])
*/
class AllsellRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'transaction_id',
        'count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Allsell::class;
    }
}
