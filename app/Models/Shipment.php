<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Shipment
 * @package App\Models
 * @version October 16, 2018, 11:16 am UTC
 *
 * @property integer postman_id
 * @property integer shipmenttype_id
 * @property integer transaction_id
 * @property string|\Carbon\Carbon send_date
 * @property string|\Carbon\Carbon delivered_date
 * @property string send_address
 * @property string delivered_address
 * @property string description
 * @property string post_code
 */
class Shipment extends Model
{
    use SoftDeletes;

    public $table = 'shipment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'postman_id',
        'shipmenttype_id',
        'transaction_id',
        'send_date',
        'delivered_date',
        'send_address',
        'delivered_address',
        'description',
        'post_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'postman_id' => 'integer',
        'shipmenttype_id' => 'integer',
        'transaction_id' => 'integer',
        'send_address' => 'string',
        'delivered_address' => 'string',
        'description' => 'string',
        'post_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function postman(){
        return $this->belongsTo('App\Models\Postman');
    }

    public function shipment_type(){
        return $this->belongsTo('App\Models\ShipmentType');
    }

    public function transaction(){
        return $this->belongsTo('App\Models\Transaction');
    }

}
