<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShipmentType
 * @package App\Models
 * @version October 16, 2018, 12:47 pm UTC
 *
 * @property string name
 */
class ShipmentType extends Model
{
    use SoftDeletes;

    public $table = 'shipmenttype';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function shipment(){
        return $this->hasmany('App\Models\Shipment');
    }
}
