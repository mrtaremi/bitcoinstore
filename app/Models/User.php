<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version October 11, 2018, 8:23 am UTC
 *
 * @property integer role_id
 * @property string email
 * @property string password
 * @property string first_name
 * @property string last_name
 * @property string company_name
 * @property string phone_number
 * @property string remember_token
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'role_id',
        'email',
        'password',
        'first_name',
        'last_name',
        'company_name',
        'phone_number',
        'const_number',
        'avatar_path',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'role_id' => 'integer',
        'email' => 'string',
        'password' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'company_name' => 'string',
        'phone_number' => 'string',
        'const_number' => 'string',
        'avatar_path' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function role(){

        return $this->belongsTo('App\Models\Role');

    }

    public function posts(){

        return $this->hasMany('App\Models\Post');

    }

}
