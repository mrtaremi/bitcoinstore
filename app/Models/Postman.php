<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Postman
 * @package App\Models
 * @version October 16, 2018, 11:17 am UTC
 *
 * @property string first_name
 * @property string last_name
 * @property string address
 * @property string phone_number
 * @property string const_number
 * @property string identity_number
 */
class Postman extends Model
{
    use SoftDeletes;

    public $table = 'postman';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'const_number',
        'user_id',
        'posttag_id',
        'identity_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'posttag_id' => 'integer',
        'user_id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'address' => 'string',
        'phone_number' => 'string',
        'const_number' => 'string',
        'identity_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function shipment(){
        return $this->hasmany('App\Models\Shipment');
    }
    
}
