<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentType
 * @package App\Models
 * @version October 16, 2018, 1:21 pm UTC
 *
 * @property string name
 */
class PaymentType extends Model
{
    use SoftDeletes;

    public $table = 'paymenttype';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function transaction(){
        return $this->hasmany('App\Models\Transaction');
    }
    
}
