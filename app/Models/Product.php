<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version October 23, 2018, 9:18 am UTC
 *
 * @property smallInteger product_type_id
 * @property smallInteger market_id
 * @property smallInteger special_service_id
 * @property integer rank
 * @property integer weight
 * @property integer off
 * @property string name
 * @property string dimensions
 * @property string colors
 * @property string gas
 * @property string main_description
 * @property string sub_description
 * @property smallInteger status
 * @property float poweruse
 * @property float btc_per_hour
 * @property float price_per_unit
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_type_id',
        'market_id',
        'special_service_id',
        'rank',
        'weight',
        'off',
        'name',
        'dimensions',
        'colors',
        'gas',
        'main_description',
        'sub_description',
        'status',
        'poweruse',
        'btc_per_hour',
        'avatar_image_path',
        'price_per_unit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'rank' => 'integer',
        'weight' => 'integer',
        'off' => 'integer',
        'name' => 'string',
        'dimensions' => 'string',
        'colors' => 'string',
        'gas' => 'string',
        'main_description' => 'string',
        'sub_description' => 'string',
        'avatar_image_path' => 'string',
        'poweruse' => 'float',
        'btc_per_hour' => 'float',
        'price_per_unit' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function imageof(){
        return $this->hasmany('App\Models\ImageOf');
    }

    
}
