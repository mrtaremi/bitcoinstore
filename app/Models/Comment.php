<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 * @package App\Models
 * @version October 27, 2018, 1:05 pm UTC
 *
 * @property integer post_id
 * @property integer product_id
 * @property integer reply_of_comment
 * @property integer likes
 * @property string value
 * @property boolean accepted
 */
class Comment extends Model
{
    use SoftDeletes;

    public $table = 'comments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'post_id',
        'product_id',
        'reply_of_comment',
        'likes',
        'value',
        'accepted'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'post_id' => 'integer',
        'product_id' => 'integer',
        'reply_of_comment' => 'integer',
        'likes' => 'integer',
        'value' => 'string',
        'accepted' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
